
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name fopid -dir "C:/git/fopid/fpga/fopid_population/fopid/planAhead_run_1" -part xc6slx150tfgg676-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/git/fopid/fpga/fopid_population/fopid/FOPID_POPULATION.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/git/fopid/fpga/fopid_population/fopid} }
set_property target_constrs_file "FOPID_POPULATION.ucf" [current_fileset -constrset]
add_files [list {FOPID_POPULATION.ucf}] -fileset [get_property constrset [current_run]]
link_design
