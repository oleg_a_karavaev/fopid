`timescale 1ns / 1ps

`define NUM_OF_CYCLES 100000

module acc_tb;

	reg clk = 1'b0;

	always #10 clk = ~clk;

	reg [73:0] data = 0;
	reg [31:0] signdata = 0;
	wire [95:0] result;

	reg sync = 1'b0;
	wire ready;

	reg init = 1'b1;

	integer randdata;
	integer randsuccess;
	integer cycle_cnt = 0;

	reg [95:0] reference_result = 0;


	always @(posedge clk) begin
		sync <= 1'b0;
		if (sync) begin
			$display("next data = %h", data);
			$display("next operation = %h", signdata[2]);
		end
		if (ready) begin
			sync <= 1'b1;
			if (signdata[2])
				reference_result = $signed(reference_result) - $signed(data);
			else
				reference_result = $signed(reference_result) + $signed(data);
			$display("mul result = %h", reference_result);
			$display("got mul result = %h", result);
			if (reference_result != result) begin
				$display("ERROR");
				$stop;
			end else
				$display("VALID");
			data = $random;
			data[31:0] <= data;
			data = $random;
			data[63:32] <= data;
			data[73:64] <= 0;
			data = $random;
			signdata <= data;
			cycle_cnt <= cycle_cnt + 1;
			if (cycle_cnt >= `NUM_OF_CYCLES-1) begin
				$display("TEST END");
				$stop;
			end

		end

		if (init) begin
			sync <= 1'b1;
			init <= 1'b0;
		end
	end

	glbl glbl();

	acc96 dut (
		  .clk_i(clk)
		, .clr_i(1'b0)
		, .data_i(data)
		, .subtract(signdata[2])
		, .sync_i(sync)
		, .acc_o(result)
		, .sync_o(ready)
	);

endmodule
