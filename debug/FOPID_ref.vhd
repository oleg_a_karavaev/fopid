-- The reference FOPID floating point model
-- This module is used for a FOPID simulation as the reference model and is not synhesizeble
-- the main idea:
-- at first need to load the weights
-- then to pass the data sample one by one sync'ed by the validation strobe. These sample will go to the data delay buffer which is to orginize a FIR-like delay line
-- When the start input is received it starts the processing in the both integral and derivative parts
-- The data samples from the data delay buffer and the weights from the weights containers is passed synchronously to the computation core of the GL modules
-- After the all multiplications are computed and accumulated for the current data delay bufer state
-- the result of the GL modules are multiplied by the K-coefficients and added together

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

entity FOPID_ref is
generic (
	  N : integer := 64 -- Window Size == Weights number
);
port (
	  clk_i : in std_logic                           -- the input clock
	; clr_i : in std_logic                           -- synchronous reset
	; data_pscl_i : in std_logic                     -- input sample prescaler signal or validation pulse that is the same
	; start_i : in std_logic                         -- start pulse - is to start the FOPID algorithm with the current state of data registers and delay line and the weights containers
	; C0 : in real                                   -- the C0 linearization constant
	; C1 : in real                                   -- the C1 linearization constant
	; H : in real                                    -- the FOPID Step
	; data_i : in real                               -- the input data samples which are sync'ed by the data_pscl_i
	; weight_pscl_i : in std_logic                   -- sync pulses for the weights loading
	; integral_weight_i : in real                    -- the weights for the integral GL-component
	; integral_weights_control_i : in std_logic      -- the integral weights validation strobe
	; derivative_weight_i : in real                  -- the weights for the derivative GL-component
	; derivative_weights_control_i : in std_logic    -- the derivative weigths validation strobe
	; valid_o : out std_logic                        -- the result validation signal. the each pulse for the each start
	; data_o : out real                              -- the FOPID result
	; Kp_i : in real                                 -- the linear path coeff
	; Ki_i : in real                                 -- the integral path coef
	; Kd_i : in real                                 -- the derivative path coef
);
end entity FOPID_ref;

architecture behav of FOPID_ref is 

	type real_vector is array (integer range <>) of real;
	signal integral_weights_container : real_vector(N-1 downto 0) := (others => 0.0);
	signal derivative_weights_container : real_vector(N-1 downto 0) := (others => 0.0);
	signal data_delay_line : real_vector(N-1 downto 0) := (others => 0.0);

	type linear_approximation_computations_t is record
		shifted : real;
		input_acc : real;
		input_acc_plus_shifted : real;
		mul_to_c1h : real;
		mul_to_c0 : real;
		acc_c1h : real;
		result : real;
	end record;

	signal la : linear_approximation_computations_t;

begin

	process (clk_i)
		variable v_la : linear_approximation_computations_t;
		variable v_integral_weight_mul, v_derivative_weight_mul : real;
		variable v_integral_result, v_derivative_result : real;
	begin
		if rising_edge(clk_i) then
			valid_o <= '0';
			-- data delay buffer
			if data_pscl_i = '1' then
				data_delay_line <= data_delay_line(data_delay_line'left-1 downto 0) & data_i;
			end if;
			-- loading the weights into the weights containers
			if weight_pscl_i = '1' then
				if integral_weights_control_i = '1' then
					integral_weights_container <= integral_weight_i & integral_weights_container(integral_weights_container'left downto 1);
				end if;
				if derivative_weights_control_i = '1' then
					derivative_weights_container <= derivative_weight_i & derivative_weights_container(derivative_weights_container'left downto 1);
				end if;
			end if;
			-- when the processing is started
			if start_i = '1' then
				-- the all accumulators and the results is intiated by 0
				v_la.input_acc := 0.0; --la.input_acc;
				v_la.acc_c1h := 0.0; --la.acc_c1h;
				v_integral_result := 0.0;
				v_derivative_result := 0.0;
				-- for the all captured samples in the data delay buffer and with the corresponding weights from the weights containers
				for i in 0 to N-1 loop
					--report "======= i = " & integer'Image(i) & " =======";
					-- the linear approximation part
					v_la.shifted := data_delay_line(i) * real(N); -- multiplication by N
					--report "v_la.shifted = " & real'Image(v_la.shifted);
					v_la.input_acc_plus_shifted := v_la.input_acc + v_la.shifted; -- ADD input data ACC to the input mupltiplied by N
					--report "v_la.input_acc_plus_shifted = " & real'Image(v_la.input_acc_plus_shifted);
					v_la.input_acc := v_la.input_acc + data_delay_line(i); -- input data ACC
					--report "v_la.input_acc = " & real'Image(v_la.input_acc);
					v_la.mul_to_c1h := C1 * H * v_la.input_acc_plus_shifted; -- Multiply by C1H
					--report "v_la.mul_to_c1h = " & real'Image(v_la.mul_to_c1h);
					v_la.mul_to_c0 := C0 * v_la.input_acc; -- input acc multiplied by C0
					--report "v_la.mul_to_c0 = " & real'Image(v_la.mul_to_c0);
					v_la.acc_c1h := v_la.acc_c1h + v_la.mul_to_c1h; -- C1H mul result ACC
					--report "v_la.acc_c1h = " & real'Image(v_la.acc_c1h);
					v_la.result := v_la.acc_c1h + v_la.mul_to_c0; -- linearization result
					--report "v_la.result = " & real'Image(v_la.result);
					---------------------------------------------------------------------------------------------
					-- applying the weights to the samples from the data delay buffer
					v_integral_weight_mul := data_delay_line(i) * integral_weights_container(i);
					--report "v_integral_weight_mul = " & real'Image(v_integral_weight_mul);
					v_derivative_weight_mul := data_delay_line(i) * derivative_weights_container(i);
					--report "v_derivative_weight_mul = " & real'Image(v_derivative_weight_mul);
					---------------------------------------------------------------------------------------------
					-- resulting accumulators for the each sample (the sum of the all processed samples which are received before and are in the data delay buffer)
					v_integral_result := v_integral_result + v_integral_weight_mul + v_la.result; -- integgral part
					--report "v_integral_result = " & real'Image(v_integral_result);
					v_derivative_result := v_derivative_result + v_derivative_weight_mul + v_la.result; -- derivative part
					--report "v_derivative_result = " & real'Image(v_derivative_result);
				end loop;
				--report "v_integral_result = " & real'Image(v_integral_result);
				--report "v_derivative_result = " & real'Image(v_derivative_result);
				data_o <= v_integral_result * Ki_i + v_derivative_result * Kd_i + data_delay_line(0) * Kp_i; -- the final FOPID result
				valid_o <= '1';
				la <= v_la;
			end if;
			if clr_i = '1' then
				la.input_acc <= 0.0;
				la.acc_c1h <= 0.0;
			end if;
		end if;
	end process;

end;