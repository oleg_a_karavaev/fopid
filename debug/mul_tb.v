`timescale 1ns / 1ps

`define NUM_OF_CYCLES 100000
// the simple multiplier test
module tb;

	reg clk = 1'b0;

	always #10 clk = ~clk;

	reg [31:0] data0 = 0, data1 = 1;
	wire [63:0] result;

	reg sync = 1'b0;
	wire ready;

	reg init = 1'b1;

	integer data;
	integer randsuccess;
	integer cycle_cnt = 0;

	reg [63:0] reference_result;

	always @(posedge clk) begin
		sync <= 1'b0;
		if (sync) begin
			$display("operand 1 = %h", data0);
			$display("operand 2 = %h", data1);
		end
		if (ready) begin
			// pass the new data and assert the sync signal
			sync <= 1'b1;
			reference_result = $signed(data0)*$signed(data1); // the reference result
			$display("mul result = %h", reference_result);
			$display("got mul result = %h", result);
			if (reference_result != result) begin // check the result
				$display("ERROR");
				$stop;
			end else
				$display("VALID");
			// generate the next data
			data = $random;
			data0 <= data;
			data = $random;
			data1 <= data;
			// test cycle counter
			cycle_cnt <= cycle_cnt + 1;
			if (cycle_cnt >= `NUM_OF_CYCLES-1) begin
				$display("TEST END");
				$stop;
			end
		end

		if (init) begin // initial (first) start
			sync <= 1'b1;
			init <= 1'b0;
		end
	end

	glbl glbl();

	mult32x32 dut (
		  .clk_i(clk)
		, .clr_i(1'b0)
		, .data0_i(data0)
		, .data1_i(data1)
		, .sync_i(sync)
		, .data_o(result)
		, .sync_o(ready)
	);

endmodule
