`timescale 1ns / 1ps

`define NUM_OF_CYCLES 100000
// weight computation module test
module weight_tb;

	reg clk = 1'b0;

	always #10 clk = ~clk;

	reg [31:0] data = (2<<23) / 1000;
	integer data_ref = 1;
	reg [31:0] signdata = 0;
	wire [31:0] result;
	integer result_int;
	real result_double;

	reg sync = 1'b0;
	wire ready;

	reg init = 1'b1;

	integer randdata;
	integer randsuccess;
	integer cycle_cnt = 0;

	real auxvar;
	reg [63:0] auxvar_int64;

	real reference_result = 0.0;

	function real exp (input real x);
	begin
		exp = 2.71828 ** x;
	end
	endfunction

	real weights [0:63];
	real weights_received [0:63];
	real weight = 1.0;
	real alpha = 0.0;
	real a = 0.0;

	integer reccnt = 0;

	wire valid;
	reg [1:0] valid_ff;
	reg complete = 1'b0;

	reg error = 1'b0;

	integer i;

	always @(posedge clk) begin
		sync <= 1'b0;
		valid_ff <= {valid_ff[0], valid};
		complete <= valid_ff[1] & ~valid_ff[0];
		if (sync) begin
			reccnt <= 0;
		end
		
		if (complete) begin
			// pass the new data and assert the sync signal
			sync <= 1'b1;
			error = 1'b0;
			if (reccnt != 64) begin // check the number of the expected weights
				$display("ERROR: expected weights 64, received weights = %d", reccnt);
				error = 1'b1;
			end
			// check the computed weights with the reference ones
			for (i = 0; i < 64; i=i+1) begin
				$display("pos %d, expected weight = %f", i, weights[i]);
				$display("pos %d, received weight = %f", i, weights_received[i]);
				auxvar = weights[i] - weights_received[i];
				if (auxvar < 0) // kind of an abs
					auxvar = -auxvar;
				if (auxvar >= 0.001) // compare the error with a some target expectations
					error = 1'b1;
			end
			if (error) begin
				$display("ERROR");
				$stop;
			end else
				$display("VALID");
			cycle_cnt <= cycle_cnt + 1;
			if (cycle_cnt >= `NUM_OF_CYCLES-1) begin
				$display("TEST END");
				$stop;
			end
		end

		if (init) begin
			sync <= 1'b1;
			init <= 1'b0;
		end

		if (ready) begin // collect the computed weights and converts the fixed point results to the floating point representation
			reccnt <= reccnt + 1;
			result_int = $signed(result);
			result_double = result_int;
			result_double = result_double / (2 << 23);
			weights_received[reccnt] <= result_double;
		end

		if (init || complete) begin // generate the new random alpha and compute the reference weights
			randdata = 0;
			while (randdata < 1 || randdata > 999) begin	
				randdata = $random;
			end
			auxvar_int64 = randdata * (2<<23);
			data[31:0] <= auxvar_int64 / 1000;// + (longint'(auxvar) % 1000 > 499 ? 1 : 0); //data0 + 1;
			data_ref <= randdata;
			a = randdata/1000.0;
			weight = 1.0;
			for (i = 0; i < 64; i=i+1) begin
				weights[i] = weight;
				if (i < 63)
					weight = (1-(a+1)/(i+1))*weight;
			end
		end
	end

	glbl glbl();

	weight_computation #(
		  .N(64)
		, .INT(8)
		, .FRC(24)
	) dut (
		  .clk_i(clk)
		, .clr_i(1'b0)
		, .start_i(sync)
		, .alpha_i(data)
		, .ready_o(ready)
		, .valid_strob_o(valid)
		, .weight_stream_o(result)
	);


endmodule