library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;
use work.random.all;

-- The FOPID testbench
-- Got the DUT FOPID and got the reference floating point FOPID and check the each other

entity fopid_tb is
end entity;

architecture behav of fopid_tb is
	
	constant N : integer := 64;
	constant INT : integer := 8;
	constant FRC : integer := 24;

	component FOPID is
	generic (
		  N : integer := 64
		; INT : integer := 8
		; FRC : integer := 24
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; data_pscl_i : in std_logic
		; start_i : in std_logic
		; C0 : in sfixed(INT-1 downto -FRC)
		; C1 : in sfixed(INT-1 downto -FRC)
		; H : in sfixed(INT-1 downto -FRC)
		; ready_to_receive_data_o : out std_logic -- is available to receive a next data
		; data_i : in sfixed(INT-1 downto -FRC)
		; weight_pscl_i : in std_logic
		; integral_weight_i : in sfixed(INT-1 downto -FRC)
		; integral_weights_control_i : in std_logic
		; derivative_weight_i : in sfixed(INT-1 downto -FRC)
		; derivative_weights_control_i : in std_logic
		; derivative_weights_containers_full_o : out std_logic
		; intergal_weights_containers_full_o : out std_logic
		; data_control_full_o : out std_logic
		; force_i : in std_logic -- 1 - ignore weights container's state
		; valid_o : out std_logic -- data valid strobes
		; data_o : out sfixed(INT-1 downto -FRC)
		; Kp_i : in sfixed(INT-1 downto -FRC)
		; Ki_i : in sfixed(INT-1 downto -FRC)
		; Kd_i : in sfixed(INT-1 downto -FRC)
	);
	end component FOPID;

	component FOPID_ref is
	generic (
		  N : integer := 64
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; data_pscl_i : in std_logic
		; start_i : in std_logic
		; C0 : in real
		; C1 : in real
		; H : in real
		; data_i : in real
		; weight_pscl_i : in std_logic
		; integral_weight_i : in real
		; integral_weights_control_i : in std_logic
		; derivative_weight_i : in real
		; derivative_weights_control_i : in std_logic
		; valid_o : out std_logic -- data valid strobes
		; data_o : out real
		; Kp_i : in real
		; Ki_i : in real
		; Kd_i : in real
	);
	end component FOPID_ref;

	signal clk : std_logic := '0';
	signal ready_to_receive_data, intergal_weights_containers_full, derivative_weights_containers_full, data_control_full, fopid_valid : std_logic;
	-- REF signals
	type ref_data_t is record
		sample : real;
		Ki : real;
		Kp : real;
		Kd : real;
		integral_weights : real;
		derivative_weights : real;
		C0 : real;
		C1 : real;
		H : real;
		result : real;
	end record;
	-- DUT signals
	signal refdata : ref_data_t;
	type sim_data_t is record
		sample : sfixed(INT-1 downto -FRC);
		Ki : sfixed(INT-1 downto -FRC);
		Kp : sfixed(INT-1 downto -FRC);
		Kd : sfixed(INT-1 downto -FRC);
		integral_weights : sfixed(INT-1 downto -FRC);
		derivative_weights : sfixed(INT-1 downto -FRC);
		C0 : sfixed(INT-1 downto -FRC);
		C1 : sfixed(INT-1 downto -FRC);
		H : sfixed(INT-1 downto -FRC);
		result : sfixed(INT-1 downto -FRC);
	end record;
	signal simdata : sim_data_t;
	-- Common controls
	type control_t is record
		start : std_logic;
		clr : std_logic;
		data_pscl : std_logic;
		weights_valid : std_logic;
	end record;

	signal ctrl : control_t := (others => '0');

	-- Testbench FSM (se below to description)
	type state_t is (IDLE, GENERATE_VECTORS, LOAD_WEIGHTS, LOAD_NEXT_DATA, START, WAIT_RESULTS, CHECK, COMPLETE);

	signal state : state_t := IDLE;

	constant TEST_NUM : integer := 100000;
	signal TESTCNT : integer := 0;
	-- impure because of random function is impure
	-- the function is need because of some addition functionality incapsulation
	impure function GetRandom(sign_en : boolean := false; bound : real := 4.0) return real is
		variable r : real;
		variable s : real;
	begin
		r := random*2.0;
		while abs(r) > bound loop -- is to bound the generated value
			r := r/10.0;
		end loop;
		if sign_en then -- sign randomization
			s := random;
			if abs(s) > 0.5 then
				r := -r;
			end if;
		end if;
		return r;
	end function;

	type real_vector is array (integer range <>) of real;
	signal integral_weights_container : real_vector(N-1 downto 0) := (others => 0.0);
	signal derivative_weights_container : real_vector(N-1 downto 0) := (others => 0.0);
	signal cnt : integer := 0;
	signal dut_valid, ref_valid : std_logic;
	signal check_result : boolean := true;
	signal err : real := 0.0;

begin

	-- FOPID under test
	DUT : FOPID
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family => "virtex5"
	)
	port map (
		  clk_i => clk
		, clr_i => ctrl.clr
		, data_pscl_i => ctrl.data_pscl
		, start_i => ctrl.start
		, C0 => simdata.C0
		, C1 => simdata.C1
		, H => simdata.H
		, ready_to_receive_data_o => ready_to_receive_data
		, data_i => simdata.sample
		, weight_pscl_i => ctrl.weights_valid
		, integral_weight_i => simdata.integral_weights
		, integral_weights_control_i => ctrl.weights_valid
		, derivative_weight_i => simdata.derivative_weights
		, derivative_weights_control_i => ctrl.weights_valid
		, derivative_weights_containers_full_o => derivative_weights_containers_full
		, intergal_weights_containers_full_o => intergal_weights_containers_full
		, data_control_full_o => data_control_full
		, force_i => '1'
		, valid_o => dut_valid
		, data_o => simdata.result
		, Kp_i => simdata.Kp
		, Ki_i => simdata.Ki
		, Kd_i => simdata.Kd
	);

	-- convert to the sfixed type to pass to the DUT (see above)
	simdata.integral_weights <= to_sfixed(refdata.integral_weights, INT-1, -FRC);
	simdata.derivative_weights <= to_sfixed(refdata.derivative_weights, INT-1, -FRC);
	simdata.sample <= to_sfixed(refdata.sample, INT-1, -FRC);
	simdata.C0 <= to_sfixed(refdata.C0, INT-1, -FRC);
	simdata.C1 <= to_sfixed(refdata.C1, INT-1, -FRC);
	simdata.H <= to_sfixed(refdata.H, INT-1, -FRC);
	simdata.Kp <= to_sfixed(refdata.Kp, INT-1, -FRC);
	simdata.Kd <= to_sfixed(refdata.Kd, INT-1, -FRC);
	simdata.Ki <= to_sfixed(refdata.Ki, INT-1, -FRC);

	-- reference floating point FOPID model
	REF : FOPID_ref
	generic map (
		  N => N
	)
	port map (
		  clk_i => clk
		, clr_i => ctrl.clr
		, data_pscl_i => ctrl.data_pscl
		, start_i => ctrl.start
		, C0 => refdata.C0
		, C1 => refdata.C1
		, H => refdata.H
		, data_i => refdata.sample
		, weight_pscl_i => ctrl.weights_valid
		, integral_weight_i => refdata.integral_weights
		, integral_weights_control_i => ctrl.weights_valid
		, derivative_weight_i => refdata.derivative_weights
		, derivative_weights_control_i => ctrl.weights_valid
		, valid_o => ref_valid
		, data_o => refdata.result
		, Kp_i => refdata.Kp
		, Ki_i => refdata.Ki
		, Kd_i => refdata.Kd
	);

	clk <= not clk after 10 ns; -- clock generation

	process (clk)
		variable v_check_result : boolean := true;
		variable v_err : real;
		variable v_tmp : real;
	begin
		if rising_edge(clk) then
			ctrl.start <= '0';
			ctrl.weights_valid <= '0';
			ctrl.data_pscl <= '0';
			ctrl.clr <= '0';
			-- 1. initial reset -> clr = 1
			-- 2. Generate test vectors: weights, Ki, Kp, Kd, C0, C1
			-- 3. Loading the weights to the both DUT and REF
			-- 4. Get the next random sample and pass it to the both DUT and REF
			-- 5. Start the processing
			-- 6. Waiting the DUT is complete (the REF will already be completed at that moment)
			-- 7. Check the result. Because of fixed poind vs floating point - check the error range
			-- 8. Goto 2 at each N*1000th test cycle or to 4 in other caseS
			case state is
				when IDLE =>
					ctrl.clr <= '1';
					state <= GENERATE_VECTORS;
				when GENERATE_VECTORS =>
					state <= LOAD_WEIGHTS;
					-- weights generation
					for i in 0 to N-1 loop
						integral_weights_container(i) <= GetRandom(true);
						derivative_weights_container(i) <= GetRandom(true);
					end loop;
					-- coefficients
					refdata.Ki <= GetRandom;
					refdata.Kp <= GetRandom;
					refdata.Kd <= GetRandom;
					refdata.C0 <= GetRandom(true, 0.001);
					refdata.C1 <= GetRandom(true, 0.001);
					refdata.H <= 0.001;
				when LOAD_WEIGHTS =>
					-- loading the weight
					ctrl.weights_valid <= '1'; -- assert validation signals
					refdata.integral_weights <= integral_weights_container(cnt);
					refdata.derivative_weights <= derivative_weights_container(cnt);
					cnt <= cnt + 1;
					if cnt = N-1 then -- upto N weights
						cnt <= 0;
						state <= LOAD_NEXT_DATA;
					end if;
				when LOAD_NEXT_DATA =>
					ctrl.data_pscl <= '1'; -- pass the data sample
					refdata.sample <= GetRandom(true);
					state <= START;
				when START =>
					state <= WAIT_RESULTS;
					ctrl.start <= '1'; -- assert the start
				when WAIT_RESULTS =>
					if dut_valid = '1' then
						state <= CHECK;
					end if;
				when CHECK =>
					v_err := refdata.result - to_real(simdata.result); -- a computatioin error
					v_check_result := abs(v_err) < abs(refdata.result*0.05) or (abs(refdata.result) < 0.3 and abs(v_err) < 0.05);
					report "CHECK:"
						severity NOTE;
					report "    reference value = " & real'Image(refdata.result)
						severity NOTE;
					report "    received value = " & real'Image(to_real(simdata.result))
						severity NOTE;
					assert v_check_result
						report "TEST FAIL: error = " & real'Image(v_err)
						severity FAILURE;
					report "RESULT IS OK: error = " & real'Image(v_err)
						severity NOTE;
					-- synchronize the result of the computation (may be useful in the waveform)
					check_result <= v_check_result;
					err <= v_err;
					TESTCNT <= TESTCNT + 1;
					if TESTCNT = TEST_NUM-1 then -- the all test cycles are passed
						state <= COMPLETE;
					else
						if (TESTCNT mod (N*1000)) = 0 then -- regenrate the test vectors at the each N*1000th test cycle
							state <= GENERATE_VECTORS;
						else
							state <= LOAD_NEXT_DATA;
						end if;
					end if;
				when COMPLETE =>
					report "TEST SUCCESS"
						severity FAILURE;
				when others =>
					state <= COMPLETE;
			end case;
		end if;
	end process;

end;