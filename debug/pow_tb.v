`timescale 1ns / 1ps

`define NUM_OF_CYCLES 100000
// the powering module test
module pow_tb;

	reg clk = 1'b0;

	always #10 clk = ~clk;

	reg [31:0] data = (2<<23) / 1000;
	integer data_ref = 1;
	reg [31:0] signdata = 0;
	wire [31:0] result;
	integer result_int;
	real result_double;

	reg sync = 1'b0;
	wire ready;

	reg init = 1'b1;

	integer randdata;
	integer randsuccess;
	integer cycle_cnt = 0;

	real auxvar;
	reg [63:0] auxvar_int64;

	real reference_result = 0.0;

	function real exp (input real x);
	begin
		exp = 2.71828 ** x;
	end
	endfunction


	always @(posedge clk) begin
		sync <= 1'b0;
		if (sync) begin
			$display("next data = %d", data);
			$display("next operation = %h", signdata[2]);
		end
		result_int = $signed(result);
		result_double = result_int;
		result_double = result_double / (2 << 23);
		if (ready) begin
			// pass the new data and assert the sync signal
			sync <= 1'b1;
			// reference_result = $ln(real'(data_ref)/1000.0);
			// reference_result = exp(real'(data_ref)/1000.0);
			reference_result = 1/(0.01**(data_ref/1000.0)); // the reference computation
			$display("ln result = %f", reference_result);
			$display("got ln result = %f", result_double);
			auxvar = reference_result - result_double; // error with floating point result
			if (auxvar < 0) // kind of an abs
				auxvar = -auxvar;
			if (auxvar >= 0.05) begin // check the error value
				$display("ERROR");
				$stop;
			end else
				$display("VALID");
			cycle_cnt <= cycle_cnt + 1;
			if (cycle_cnt >= `NUM_OF_CYCLES-1) begin
				$display("TEST END");
				$stop;
			end

		end

		if (init) begin
			sync <= 1'b1;
			init <= 1'b0;
		end

		if (init || ready) begin // get the next random data
			randdata = 0;
			while (randdata < 1 || randdata > 999) begin	
				randdata = $random;
			end
			auxvar_int64 = randdata * (2<<23);
			data[31:0] <= auxvar_int64 / 1000;// + (longint'(auxvar) % 1000 > 499 ? 1 : 0); //data0 + 1;
			data_ref <= randdata;
		end
	end

	glbl glbl();

	powering_operation_single #(
		  .INT(8)
		, .FRC(24)
		// , .h(-77261)
	) dut (
		  .clk_i(clk)
		, .clr_i(1'b0)
		, .start_i(sync)
		, .ready_o(ready)
		, .argument_i(data)//-(2<<23))
		, .result_o(result)
	);

endmodule
