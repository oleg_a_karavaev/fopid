library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;
use work.random.all;

-- The FOPID testbench
-- Got the DUT FOPID and got the reference floating point FOPID and check the each other

entity fopid_population_tb is
end entity;

architecture behav of fopid_population_tb is
	
	constant N : integer := 64;
	constant INT : integer := 8;
	constant FRC : integer := 24;
	constant POPULATION_SIZE : integer := 50;

	component FOPID_POPULATION is
	generic (
		  N : integer := 64
		; INT : integer := 8
		; FRC : integer := 24
		; POPULATION_SIZE : integer := 200
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; start_i : in std_logic
		; current_population_size : in std_logic_vector(integer(log2(real(POPULATION_SIZE)))-1 downto 0)
		; data_pscl_i : in std_logic
		; data_control_full_o : out std_logic
		; data_i : in sfixed(INT-1 downto -FRC)
		; ready_to_receive_data_o : out std_logic -- is available to receive a next data
		; population_input_control_enable_i : in std_logic_vector(POPULATION_SIZE-1 downto 0)
		; integral_weight_pscl_i : in std_logic
		; derivative_weight_pscl_i : in std_logic
		; integral_weight_i : in sfixed(INT-1 downto -FRC)
		; derivative_weight_i : in sfixed(INT-1 downto -FRC)
		; derivative_weights_containers_full_o : out std_logic_vector(POPULATION_SIZE-1 downto 0)
		; intergal_weights_containers_full_o : out std_logic_vector(POPULATION_SIZE-1 downto 0)
		; K_we_i : in std_logic_vector(2 downto 0)
		; Kp_i : in sfixed(INT-1 downto -FRC)
		; Ki_i : in sfixed(INT-1 downto -FRC)
		; Kd_i : in sfixed(INT-1 downto -FRC)
		; C_we_i : in std_logic_vector(1 downto 0)
		; C0_i : in sfixed(INT-1 downto -FRC)
		; C1_i : in sfixed(INT-1 downto -FRC)
		; H_i : in sfixed(INT-1 downto -FRC)
		; force_i : in std_logic -- 1 - ignore weights container's state
		; valid_o : out std_logic_vector(POPULATION_SIZE-1 downto 0)
		; data_o : out sfixed(INT-1 downto -FRC)
	);
	end component FOPID_POPULATION;

	component FOPID_ref is
	generic (
		  N : integer := 64
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; data_pscl_i : in std_logic
		; start_i : in std_logic
		; C0 : in real
		; C1 : in real
		; H : in real
		; data_i : in real
		; weight_pscl_i : in std_logic
		; integral_weight_i : in real
		; integral_weights_control_i : in std_logic
		; derivative_weight_i : in real
		; derivative_weights_control_i : in std_logic
		; valid_o : out std_logic -- data valid strobes
		; data_o : out real
		; Kp_i : in real
		; Ki_i : in real
		; Kd_i : in real
	);
	end component FOPID_ref;

	signal clk : std_logic := '0';
	signal ready_to_receive_data, data_control_full, fopid_valid : std_logic;
	signal intergal_weights_containers_full, derivative_weights_containers_full : std_logic_vector(POPULATION_SIZE-1 downto 0);
	-- REF signals
	type ref_data_t is record
		sample : real;
		Ki : real;
		Kp : real;
		Kd : real;
		integral_weights : real;
		derivative_weights : real;
		C0 : real;
		C1 : real;
		H : real;
		result : real;
	end record;
	-- DUT signals
	signal refdata : ref_data_t;
	type sim_data_t is record
		sample : sfixed(INT-1 downto -FRC);
		Ki : sfixed(INT-1 downto -FRC);
		Kp : sfixed(INT-1 downto -FRC);
		Kd : sfixed(INT-1 downto -FRC);
		integral_weights : sfixed(INT-1 downto -FRC);
		derivative_weights : sfixed(INT-1 downto -FRC);
		C0 : sfixed(INT-1 downto -FRC);
		C1 : sfixed(INT-1 downto -FRC);
		H : sfixed(INT-1 downto -FRC);
		result : sfixed(INT-1 downto -FRC);
	end record;
	signal simdata : sim_data_t;
	-- Common controls
	type control_t is record
		start : std_logic;
		clr : std_logic;
		data_pscl : std_logic;
		weights_valid : std_logic;
		population_ctrl_en : std_logic_vector(POPULATION_SIZE-1 downto 0);
		coefficients_en : std_logic;
		constants_en : std_logic;
		enable : std_logic_vector(1 downto 0);
	end record;

	signal ctrl : control_t := (enable => "00", population_ctrl_en => (others => '0'), others => '0');

	-- Testbench FSM (se below to description)
	type state_t is (
		IDLE, GENERATE_POPULATION_SIZE, GENERATE_VECTORS, LOAD_WEIGHTS, LOAD_NEXT_DATA, START, WAIT_RESULTS,
		COMPUTE_REFERENCE_INIT, COMPUTE_REFERENCE_START, COMPUTE_REFERENCE_LOAD_WEIGHTS, COMPUTE_REFERENCE_GET_RESULT,
		CHECK, COMPLETE
	);

	signal state : state_t := IDLE;

	constant TEST_NUM : integer := 100000;
	signal TESTCNT : integer := 0;
	-- impure because of random function is impure
	-- the function is need because of some addition functionality incapsulation
	impure function GetRandom(sign_en : boolean := false; bound : real := 4.0) return real is
		variable r : real;
		variable s : real;
	begin
		r := random*2.0;
		while abs(r) > bound loop -- is to bound the generated value
			r := r/10.0;
		end loop;
		if sign_en then -- sign randomization
			s := random;
			if abs(s) > 0.5 then
				r := -r;
			end if;
		end if;
		return r;
	end function;

	impure function GetRandom(sign_en : boolean := false; bound : integer := 200; low_bound : integer := 0) return integer is
		variable r : integer;
		variable s : real;
	begin
		r := random;
		while abs(r) > bound loop -- is to bound the generated value
			r := r/10;
		end loop;
		while abs(r) < low_bound loop -- is to bound the generated value
			r := r*10;
		end loop;
		if sign_en then -- sign randomization
			s := random;
			if abs(s) > 0.5 then
				r := -r;
			end if;
		end if;
		return r;
	end function;

	type real_vector is array (integer range <>) of real;
	subtype weights_vector is real_vector(N-1 downto 0);
	type weights_matrix is array (integer range <>) of weights_vector;
	signal integral_weights_container : weights_matrix(POPULATION_SIZE-1 downto 0) := (others => (others => 0.0));
	signal derivative_weights_container : weights_matrix(POPULATION_SIZE-1 downto 0) := (others => (others => 0.0));
	signal Kp_container : real_vector(POPULATION_SIZE-1 downto 0) := (others => 0.0);
	signal Kd_container : real_vector(POPULATION_SIZE-1 downto 0) := (others => 0.0);
	signal Ki_container : real_vector(POPULATION_SIZE-1 downto 0) := (others => 0.0);
	signal C1_container : real_vector(POPULATION_SIZE-1 downto 0) := (others => 0.0);
	signal C0_container : real_vector(POPULATION_SIZE-1 downto 0) := (others => 0.0);
	signal cnt, pcnt : integer := 0;
	signal ref_valid : std_logic;
	signal dut_valid : std_logic_vector(POPULATION_SIZE-1 downto 0);
	signal check_result : boolean := true;
	signal err : real := 0.0;
	signal current_population_size : unsigned(integer(log2(real(POPULATION_SIZE)))-1 downto 0) := to_unsigned(POPULATION_SIZE-1, integer(log2(real(POPULATION_SIZE))));


	signal ref_results_container : real_vector(POPULATION_SIZE-1 downto 0) := (others => 0.0);
	type sfixed_vector is array (integer range <>) of sfixed(INT-1 downto -FRC);
	signal dut_results_container : sfixed_vector(POPULATION_SIZE-1 downto 0) := (others => to_sfixed(0.0, INT-1, -FRC));

	signal dut_pcnt, ref_pcnt :integer := 0;
begin

	-- FOPID under test
	DUT : FOPID_POPULATION
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, POPULATION_SIZE => POPULATION_SIZE
		, family => "spartan6"
	)
	port map (
		  clk_i => clk
		, clr_i => ctrl.clr
		, data_pscl_i => ctrl.data_pscl
		, start_i => ctrl.start and ctrl.enable(0)
		, current_population_size => std_logic_vector(current_population_size)
		, C_we_i =>  (others => ctrl.coefficients_en and ctrl.enable(0))
		, C0_i => simdata.C0
		, C1_i => simdata.C1
		, H_i => simdata.H
		, ready_to_receive_data_o => ready_to_receive_data
		, data_i => simdata.sample
		, population_input_control_enable_i => ctrl.population_ctrl_en
		, integral_weight_pscl_i => ctrl.weights_valid and ctrl.enable(0)
		, derivative_weight_pscl_i => ctrl.weights_valid and ctrl.enable(0)
		, integral_weight_i => simdata.integral_weights
		--, integral_weights_control_i => ctrl.weights_valid and ctrl.enable(0)
		, derivative_weight_i => simdata.derivative_weights
		--, derivative_weights_control_i => ctrl.weights_valid and ctrl.enable(0)
		, derivative_weights_containers_full_o => derivative_weights_containers_full
		, intergal_weights_containers_full_o => intergal_weights_containers_full
		, data_control_full_o => data_control_full
		, force_i => '1'
		, valid_o => dut_valid
		, data_o => simdata.result
		, K_we_i => (others => ctrl.coefficients_en and ctrl.enable(0))
		, Kp_i => simdata.Kp
		, Ki_i => simdata.Ki
		, Kd_i => simdata.Kd
	);

	simdata.sample <= to_sfixed(refdata.sample, INT-1, -FRC);

	-- reference floating point FOPID model
	REF : FOPID_ref
	generic map (
		  N => N
	)
	port map (
		  clk_i => clk
		, clr_i => ctrl.clr
		, data_pscl_i => ctrl.data_pscl
		, start_i => ctrl.start and ctrl.enable(1)
		, C0 => refdata.C0
		, C1 => refdata.C1
		, H => refdata.H
		, data_i => refdata.sample
		, weight_pscl_i => ctrl.weights_valid and ctrl.enable(1)
		, integral_weight_i => refdata.integral_weights
		, integral_weights_control_i => ctrl.weights_valid and ctrl.enable(1)
		, derivative_weight_i => refdata.derivative_weights
		, derivative_weights_control_i => ctrl.weights_valid and ctrl.enable(1)
		, valid_o => ref_valid
		, data_o => refdata.result
		, Kp_i => refdata.Kp
		, Ki_i => refdata.Ki
		, Kd_i => refdata.Kd
	);

	clk <= not clk after 10 ns; -- clock generation

	process (clk)
		variable v_check_result : boolean := true;
		variable v_err : real;
		variable v_tmp : real;
		variable v_tmp_int : integer;
	begin
		if rising_edge(clk) then
			ctrl.start <= '0';
			ctrl.weights_valid <= '0';
			ctrl.data_pscl <= '0';
			ctrl.clr <= '0';
			ctrl.coefficients_en <= '0';
			ctrl.population_ctrl_en <= (others => '0');
			-- 1. initial reset -> clr = 1
			-- 2. Generate test vectors: weights, Ki, Kp, Kd, C0, C1
			-- 3. Loading the weights to the both DUT and REF
			-- 4. Get the next random sample and pass it to the both DUT and REF
			-- 5. Start the processing
			-- 6. Waiting the DUT is complete (the REF will already be completed at that moment)
			-- 7. Check the result. Because of fixed poind vs floating point - check the error range
			-- 8. Goto 2 at each N*1000th test cycle or to 4 in other caseS
			case state is
				when IDLE =>
					ctrl.clr <= '1';
					state <= GENERATE_POPULATION_SIZE;
				when GENERATE_POPULATION_SIZE =>
					v_tmp_int := GetRandom(false, POPULATION_SIZE, POPULATION_SIZE/10);
					current_population_size <= to_unsigned(v_tmp_int, integer(log2(real(POPULATION_SIZE))));
					state <= GENERATE_VECTORS;
				when GENERATE_VECTORS =>
					state <= LOAD_WEIGHTS;
					-- weights generation
					for j in 0 to POPULATION_SIZE-1 loop
						if j < to_integer(current_population_size) then
							for i in 0 to N-1 loop
								integral_weights_container(j)(i) <= GetRandom(true);
								derivative_weights_container(j)(i) <= GetRandom(true);
							end loop;
							Ki_container(j) <= GetRandom;
							Kp_container(j) <= GetRandom;
							Kd_container(j) <= GetRandom;
							C0_container(j) <= GetRandom(true, 0.001);
							C1_container(j) <= GetRandom(true, 0.001);
						else
							for i in 0 to N-1 loop
								integral_weights_container(j)(i) <= 0.0;
								derivative_weights_container(j)(i) <= 0.0;
							end loop;
							Ki_container(j) <= 0.0;
							Kp_container(j) <= 0.0;
							Kd_container(j) <= 0.0;
							C0_container(j) <= 0.0;
							C1_container(j) <= 0.0;
						end if;
					end loop;
					refdata.H <= 0.001;
				when LOAD_WEIGHTS =>
					-- loading the weight
					ctrl.weights_valid <= '1'; -- assert validation signals
					ctrl.enable <= "01";
					ctrl.population_ctrl_en(pcnt) <= '1';
					simdata.integral_weights <= to_sfixed(integral_weights_container(pcnt)(cnt), INT-1, -FRC);
					simdata.derivative_weights <= to_sfixed(derivative_weights_container(pcnt)(cnt), INT-1, -FRC);
					simdata.C0 <= to_sfixed(C0_container(pcnt), INT-1, -FRC);
					simdata.C1 <= to_sfixed(C1_container(pcnt), INT-1, -FRC);
					simdata.H <= to_sfixed(refdata.H, INT-1, -FRC);
					simdata.Kp <= to_sfixed(Kp_container(pcnt), INT-1, -FRC);
					simdata.Kd <= to_sfixed(Kd_container(pcnt), INT-1, -FRC);
					simdata.Ki <= to_sfixed(Ki_container(pcnt), INT-1, -FRC);
					if cnt = 0 then
						ctrl.coefficients_en <= '1';
					end if;
					cnt <= cnt + 1;
					if cnt = N-1 then -- upto N weights
						cnt <= 0;
						pcnt <= pcnt + 1;
						if (pcnt = to_integer(current_population_size)-1) then
							pcnt <= 0;
							state <= LOAD_NEXT_DATA;
						end if;
					end if;
				when LOAD_NEXT_DATA =>
					ctrl.data_pscl <= '1'; -- pass the data sample
					refdata.sample <= GetRandom(true);
					state <= START;
				when START =>
					state <= WAIT_RESULTS;
					ctrl.start <= '1'; -- assert the start
					ref_pcnt <= 0;
					dut_pcnt <= 0;
				when WAIT_RESULTS =>
					if dut_valid(dut_pcnt) = '1' then
						dut_results_container(dut_pcnt) <= simdata.result;
						dut_pcnt <= dut_pcnt + 1;
						if (dut_pcnt = to_integer(current_population_size)-1) then
							state <= COMPUTE_REFERENCE_INIT;
							pcnt <= 0;
						end if;
					end if;
				when COMPUTE_REFERENCE_INIT =>
					ctrl.enable <= "10";
					pcnt <= 0;
					state <= COMPUTE_REFERENCE_LOAD_WEIGHTS;
				when COMPUTE_REFERENCE_LOAD_WEIGHTS =>
					ctrl.weights_valid <= '1'; -- assert validation signals
					refdata.integral_weights <= integral_weights_container(pcnt)(cnt);
					refdata.derivative_weights <= derivative_weights_container(pcnt)(cnt);
					refdata.C0 <= C0_container(pcnt);
					refdata.C1 <= C1_container(pcnt);
					refdata.Ki <= Ki_container(pcnt);
					refdata.Kp <= Kp_container(pcnt);
					refdata.Kd <= Kd_container(pcnt);
					if cnt = 0 then
						ctrl.coefficients_en <= '1';
					end if;
					cnt <= cnt + 1;
					if cnt = N-1 then -- upto N weights
						cnt <= 0;
						state <= COMPUTE_REFERENCE_START;
					end if;
				when COMPUTE_REFERENCE_START =>
					ctrl.start <= '1';
					state <= COMPUTE_REFERENCE_GET_RESULT;
				when COMPUTE_REFERENCE_GET_RESULT =>
					if ref_valid = '1' then
						ref_results_container(pcnt) <= refdata.result;
						pcnt <= pcnt + 1;
						if (pcnt = to_integer(current_population_size)-1) then
							pcnt <= 0;
							state <= CHECK;
						else
							state <= COMPUTE_REFERENCE_LOAD_WEIGHTS;
						end if;
					end if;
				when CHECK =>
					check_loop : for j in 0 to POPULATION_SIZE-1 loop
						if j < to_integer(current_population_size) then
							v_err := ref_results_container(pcnt) - to_real(dut_results_container(pcnt)); -- a computatioin error
							v_check_result := abs(v_err) < abs(ref_results_container(pcnt)*0.05) or (abs(ref_results_container(pcnt)) < 0.3 and abs(v_err) < 0.05); -- if the error is greater then 1% of the result - the check is fail
							report "CHECK:"
								severity NOTE;
							report "    reference value = " & real'Image(ref_results_container(pcnt))
								severity NOTE;
							report "    received value = " & real'Image(to_real(dut_results_container(pcnt)))
								severity NOTE;
							assert v_check_result
								report "TEST FAIL: error = " & real'Image(v_err)
								severity FAILURE;
							report "RESULT IS OK: error = " & real'Image(v_err)
								severity NOTE;
							-- synchronize the result of the computation (may be useful in the waveform)
							check_result <= v_check_result;
							err <= v_err;							
						else
							next check_loop;
						end if;
					end loop;
					TESTCNT <= TESTCNT + 1;
					if TESTCNT = TEST_NUM-1 then -- the all test cycles are passed
						state <= COMPLETE;
					else
						if (TESTCNT mod (N*1000)) = 0 then -- regenrate the test vectors at the each N*1000th test cycle
							state <= GENERATE_VECTORS;
						else
							state <= LOAD_NEXT_DATA;
							ctrl.enable <= "01";
						end if;
					end if;
				when COMPLETE =>
					report "TEST SUCCESS"
						severity FAILURE;
				when others =>
					state <= COMPLETE;
			end case;
		end if;
	end process;

end;