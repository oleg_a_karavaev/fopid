library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

entity FOPID_POPULATION is
generic (
	  N : integer := 64
	; INT : integer := 8
	; FRC : integer := 24
	; POPULATION_SIZE : integer := 50
	; family : string := "spartan6"
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic
	; start_i : in std_logic
	; current_population_size : in std_logic_vector(integer(log2(real(POPULATION_SIZE)))-1 downto 0)
	; data_pscl_i : in std_logic
	; data_control_full_o : out std_logic
	; data_i : in sfixed(INT-1 downto -FRC)
	; ready_to_receive_data_o : out std_logic -- is available to receive a next data
	; population_input_control_enable_i : in std_logic_vector(POPULATION_SIZE-1 downto 0)
	; integral_weight_pscl_i : in std_logic
	; derivative_weight_pscl_i : in std_logic
	; integral_weight_i : in sfixed(INT-1 downto -FRC)
	; derivative_weight_i : in sfixed(INT-1 downto -FRC)
	; derivative_weights_containers_full_o : out std_logic_vector(POPULATION_SIZE-1 downto 0)
	; intergal_weights_containers_full_o : out std_logic_vector(POPULATION_SIZE-1 downto 0)
	; K_we_i : in std_logic_vector(2 downto 0)
	; Kp_i : in sfixed(INT-1 downto -FRC)
	; Ki_i : in sfixed(INT-1 downto -FRC)
	; Kd_i : in sfixed(INT-1 downto -FRC)
	; C_we_i : in std_logic_vector(1 downto 0)
	; C0_i : in sfixed(INT-1 downto -FRC)
	; C1_i : in sfixed(INT-1 downto -FRC)
	; H_i : in sfixed(INT-1 downto -FRC)
	; force_i : in std_logic -- 1 - ignore weights container's state
	; valid_o : out std_logic_vector(POPULATION_SIZE-1 downto 0)
	; data_o : out sfixed(INT-1 downto -FRC)
);
end entity FOPID_POPULATION;

architecture rtl of FOPID_POPULATION is

	component FOPID is
	generic (
		  N : integer := 64
		; INT : integer := 8
		; FRC : integer := 24
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; data_pscl_i : in std_logic
		; start_i : in std_logic
		; C0 : in sfixed(INT-1 downto -FRC)
		; C1 : in sfixed(INT-1 downto -FRC)
		; H : in sfixed(INT-1 downto -FRC)
		; ready_to_receive_data_o : out std_logic -- is available to receive a next data
		; data_i : in sfixed(INT-1 downto -FRC)
		; weight_pscl_i : in std_logic
		; integral_weight_i : in sfixed(INT-1 downto -FRC)
		; integral_weights_control_i : in std_logic
		; derivative_weight_i : in sfixed(INT-1 downto -FRC)
		; derivative_weights_control_i : in std_logic
		; derivative_weights_containers_full_o : out std_logic
		; intergal_weights_containers_full_o : out std_logic
		; data_control_full_o : out std_logic
		; force_i : in std_logic -- 1 - ignore weights container's state
		; valid_o : out std_logic -- data valid strobes
		; data_o : out sfixed(INT-1 downto -FRC)
		; Kp_i : in sfixed(INT-1 downto -FRC)
		; Ki_i : in sfixed(INT-1 downto -FRC)
		; Kd_i : in sfixed(INT-1 downto -FRC)
	);
	end component FOPID;

	component data_feeding_control is
	generic (
		  N : integer := 64     -- A number of the weights - i.e. delay stages
		; INT : integer := 8
		; FRC : integer := 24
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; restart_i : in std_logic
		; data_pscl_i : in std_logic
		; ready_o : out std_logic -- is available to receive a next data
		; empty_o : out std_logic
		; start_i : in std_logic  -- start to work
		; data_i : in sfixed(INT-1 downto -FRC)
		; complete_o : out std_logic -- data feeding to an extarnal module is complete
		; ready_i : in std_logic  -- an external module ready to get a new data
		; valid_o : out std_logic -- data valid strobe
		; data_o : out sfixed(INT-1 downto -FRC)
	);	
	end component data_feeding_control;

	component or_recursive is
	generic (
		  order : integer := 1 -- the current order of the "tree" - actually the width of the input data
	);
	port (
		  a : in std_logic_vector(order-1 downto 0)
		; z : out std_logic
	);
	end component or_recursive;

	signal population_cnt : unsigned(integer(log2(real(POPULATION_SIZE)))-1 downto 0) := (others => '0');
	signal single_fopid_run_enable, integral_weights_valid_any, start_single_fopid, data_control_full, ready_to_receive_data : std_logic;
	signal weights_valid, get_next_population : std_logic;
	signal Kp, Ki, Kd, integral_weights_mux, derivative_weights_mux : sfixed(INT-1 downto -FRC);
	signal integral_weight_control_empty_vector, derivative_weight_control_empty_vector : std_logic_vector(POPULATION_SIZE-1 downto 0);
	signal integral_weight_control_complete_vector, derivative_weight_control_complete_vector : std_logic_vector(POPULATION_SIZE-1 downto 0);
	signal integral_weights_valid, derivative_weights_valid : std_logic_vector(POPULATION_SIZE-1 downto 0);
	type sfixed_vector is array (integer range <>) of sfixed(INT-1 downto -FRC);
	signal integral_weights, derivative_weights : sfixed_vector(POPULATION_SIZE-1 downto 0);
	signal valid_single_fopid : std_logic;
	signal data_single_fopid : sfixed(INT-1 downto -FRC);
	signal start_vector : std_logic_vector(POPULATION_SIZE-1 downto 0);

	type state_t is (IDLE, INIT_WEIGHTS, WAIT_WEIGHTS_INIT_COMPLETE, RUN_FOPID, WAIT_FOPID_COMPLETE, FINALIZE);
	signal state : state_t := IDLE;

	signal C0, C1, H : sfixed(INT-1 downto -FRC);
	signal derivative_weights_containers_full, intergal_weights_containers_full : std_logic;
	signal restart : std_logic := '0';
begin

	Kp_container : data_feeding_control
	generic map (
		  N => POPULATION_SIZE
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => restart
		, data_pscl_i => K_we_i(0)
		, ready_o => open
		, start_i => start_i and single_fopid_run_enable
		, data_i => Kp_i
		, complete_o => open
		, ready_i => get_next_population
		, valid_o => open
		, data_o => Kp
	);

	Ki_container : data_feeding_control
	generic map (
		  N => POPULATION_SIZE
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => restart
		, data_pscl_i => K_we_i(1)
		, ready_o => open
		, start_i => start_i and single_fopid_run_enable
		, data_i => Ki_i
		, complete_o => open
		, ready_i => get_next_population
		, valid_o => open
		, data_o => Ki
	);

	Kd_container : data_feeding_control
	generic map (
		  N => POPULATION_SIZE
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => restart
		, data_pscl_i => K_we_i(2)
		, ready_o => open
		, start_i => start_i and single_fopid_run_enable
		, data_i => Kd_i
		, complete_o => open
		, ready_i => get_next_population
		, valid_o => open
		, data_o => Kd
	);

	C1_container : data_feeding_control
	generic map (
		  N => POPULATION_SIZE
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => restart
		, data_pscl_i => C_we_i(1)
		, ready_o => open
		, start_i => start_i and single_fopid_run_enable
		, data_i => C1_i
		, complete_o => open
		, ready_i => get_next_population
		, valid_o => open
		, data_o => C1
	);

	C0_container : data_feeding_control
	generic map (
		  N => POPULATION_SIZE
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => restart
		, data_pscl_i => C_we_i(0)
		, ready_o => open
		, start_i => start_i and single_fopid_run_enable
		, data_i => C0_i
		, complete_o => open
		, ready_i => get_next_population
		, valid_o => open
		, data_o => C0
	);

	weights_containers_gen: for i in 0 to POPULATION_SIZE-1 generate
		intergal_weights_containers_full_o(i) <= not integral_weight_control_empty_vector(i);
		derivative_weights_containers_full_o(i) <= not derivative_weight_control_empty_vector(i);

		integral_weight_control : data_feeding_control
		generic map (
			  N => N
			, INT => INT
			, FRC => FRC
			, family => family
		)
		port map (
			  clk_i => clk_i
			, clr_i => clr_i
			, restart_i => '0'
			, data_pscl_i => integral_weight_pscl_i and population_input_control_enable_i(i)
			, empty_o => integral_weight_control_empty_vector(i)
			, ready_o => open
			, start_i => start_vector(i)
			, data_i => integral_weight_i
			, complete_o => integral_weight_control_complete_vector(i)
			, ready_i => '1'
			, valid_o => integral_weights_valid(i)
			, data_o => integral_weights(i)
		);

		derivative_weight_control : data_feeding_control
		generic map (
			  N => N
			, INT => INT
			, FRC => FRC
			, family => family
		)
		port map (
			  clk_i => clk_i
			, clr_i => clr_i
			, restart_i => '0'
			, data_pscl_i => derivative_weight_pscl_i and population_input_control_enable_i(i)
			, empty_o => derivative_weight_control_empty_vector(i)
			, ready_o => open
			, start_i => start_vector(i)
			, data_i => derivative_weight_i
			, complete_o => derivative_weight_control_complete_vector(i)
			, ready_i => '1'
			, valid_o => derivative_weights_valid(i)
			, data_o => derivative_weights(i)
		);

	end generate;


	FOPID_inst : FOPID
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data_pscl_i => data_pscl_i
		, C0 => C0
		, C1 => C1
		, H => H_i
		, start_i => start_single_fopid
		, ready_to_receive_data_o => ready_to_receive_data
		, data_i => data_i
		, weight_pscl_i => weights_valid
		, integral_weight_i => integral_weights_mux
		, integral_weights_control_i => weights_valid
		, derivative_weight_i => derivative_weights_mux
		, derivative_weights_control_i => weights_valid
		, derivative_weights_containers_full_o => derivative_weights_containers_full
		, intergal_weights_containers_full_o => intergal_weights_containers_full
		, data_control_full_o => data_control_full
		, force_i => force_i
		, valid_o => valid_single_fopid
		, data_o => data_single_fopid
		, Kp_i => Kp
		, Ki_i => Ki
		, Kd_i => Kd
	);

	weights_valid_or : or_recursive
	generic map (
		  order => POPULATION_SIZE
	)
	port map (
		  a => integral_weights_valid
		, z => integral_weights_valid_any
	);
	

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			single_fopid_run_enable <= force_i or data_control_full;
			if start_i = '1' then
				if single_fopid_run_enable = '1' then
					state <= INIT_WEIGHTS;
				end if;
			end if;
			restart <= '0';
			start_vector <= (others => '0');
			start_single_fopid <= '0';
			derivative_weights_mux <= derivative_weights(to_integer(population_cnt));
			integral_weights_mux <= integral_weights(to_integer(population_cnt));
			weights_valid <= integral_weights_valid_any;
			get_next_population <= '0';
			valid_o <= (others => '0');
			case state is
				when IDLE =>
					population_cnt <= (others => '0');
				when INIT_WEIGHTS =>
					start_vector(to_integer(population_cnt)) <= '1';
					state <= WAIT_WEIGHTS_INIT_COMPLETE;
					get_next_population <= '1';
				when WAIT_WEIGHTS_INIT_COMPLETE =>
					if integral_weight_control_complete_vector(to_integer(population_cnt)) = '1' then
					--if intergal_weights_containers_full = '1' then
						state <= RUN_FOPID;
					end if;
				when RUN_FOPID =>
					start_single_fopid <= '1';
					state <= WAIT_FOPID_COMPLETE;
				when WAIT_FOPID_COMPLETE =>
					if valid_single_fopid = '1' then
						state <= FINALIZE;
						valid_o(to_integer(population_cnt)) <= '1';
						data_o <= data_single_fopid;
					end if;
				when FINALIZE =>
					population_cnt <= population_cnt + 1;
					if population_cnt = unsigned(current_population_size) then
						state <= IDLE;
						restart <= '1';
					else
						state <= INIT_WEIGHTS;
					end if;
				when others =>
					state <= IDLE;
					restart <= '1';
			end case;
			if clr_i = '1' then
				state <= IDLE;
				population_cnt <= (others => '0');
				single_fopid_run_enable <= '0';
			end if;
		end if;
	end process;


end;