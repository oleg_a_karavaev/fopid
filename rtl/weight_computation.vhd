-- A weights computation module
-- W[j] = (1-(a+1)/j)*W[j-1], W[0] = 1.0
-- a - is the port alpha_i
-- fixed point implementation
-- Author: Oleg Karavaev

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;
use std.textio.all; 

entity weight_computation is 
generic (
	  N : integer := 64     -- A number of the weights
	; INT : integer := 8
	; FRC : integer := 24
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic
	; start_i : in std_logic                          -- start pulse. high level starts the computation
	; alpha_i : in sfixed(INT-1 downto -FRC)          -- an argument
	; ready_o : out std_logic                         -- ready pulse, some kind of a valid pulse, the current result's sample is valid and must be read when this pulse is high
	; valid_strob_o : out std_logic                   -- a valid output strobe. When high the output results stream is valid and the all included ready_o are valid too
	; weight_stream_o : out sfixed(INT-1 downto -FRC) -- an output results stream
);
-- clk_i           _|`|__|`|__|`|__|`|__|`|__|`|__|`|__|`|__|`|__|`|__|`| ...  _|`|__|`|__|`|_
-- start_i         ___|``|______________________________________________  ...  _________________
-- alpha_i         ---<val>---------------------------------------------  ...  -----------------
-- ready_o         _____________________|````|______________|````|______  ...  _|````|_________
-- valid_strob_o   ___________________|`````````````````````````````````  ...  ```````````|_____
-- weight_stream_o ---------------------<W[0]>--------------<W[1]>------  ...  -<W[n-1]>------- 

end entity;

architecture rtl of weight_computation is

	signal weight, weight_net, operand1, operand2 : sfixed(INT-1 downto -FRC) := to_sfixed(1.0, INT-1, -FRC);

	type sfixed_vector is array (integer range <>) of sfixed(INT-1 downto -FRC);

	-- 1/j queue generation
	function generate_inversely_proportional(constant N : integer; constant INT : integer := 8; constant FRC : integer := 24) return sfixed_vector is
		variable v_decimal : real := 1.0;
		variable v_sfixed : sfixed(INT-1 downto -FRC);
		variable r : sfixed_vector(0 to N-1);
	begin
		for i in 1 to N loop
			v_decimal := 1.0/real(i);
			v_sfixed := to_sfixed(v_decimal, INT-1, -FRC);
			r(i-1) := v_sfixed;
		end loop;
		return r;
	end function;
	-- sequence of N 1/j constants
	constant inversely_proportional_coefficients : sfixed_vector(0 to N-1) := generate_inversely_proportional(N, INT, FRC);

	component mult32x32 is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(31 downto 0)
		; data1_i: in std_logic_vector(31 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(63 downto 0)
		; sync_o: out std_logic
	);
	end component mult32x32;

	signal alpha_plus_one : sfixed(INT-1 downto -FRC);
	signal alpha_plus_one_div_j : sfixed(INT-1 downto -FRC);
	signal full_coef : sfixed(INT-1 downto -FRC);
	signal job : std_logic := '0';
	signal cnt : integer := 0;
	signal queue1_en, queue2_en, queue3_en, queue4_en : std_logic := '0';
	signal alpha : sfixed(INT-1 downto -FRC);

	-- A processing state: IDLE -> INIT (compute a+1, reset current weight to W[0] = 1) ->
	-- STEP1 (init mul by 1/j) -> STEP1_WAIT (wait mul ready) ->
	-- STEP2 (compute the full coef 1-(a+1)/j) -> STEP3 (init mul by W[j-1]) ->
	-- STEP3_WAIT (wait mul ready, output the result, check the counter and either repeat from STATE1 or goto IDLE)
	type state_t is (IDLE, INIT, STEP1, STEP1_WAIT, STEP2, STEP3, STEP3_WAIT);
	signal state : state_t := IDLE;
	signal mul_sync, mul_rdy : std_logic := '0';
	signal mul_result : std_logic_vector(63 downto 0);
	signal first_val_sent : std_logic := '0';
	signal mul_rdy_ff : std_logic_vector(1 downto 0);
begin
	
	-- this multiplier is for multiplication both by 1/j and by W[j-1] - regarding the current state
	inv_mul : mult32x32
	port map(
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => to_slv(operand1)
		, data1_i => to_slv(operand2)
		, sync_i => mul_sync
		, data_o => mul_result
		, sync_o => mul_rdy
	);

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			mul_sync <= '0';
			ready_o <= '0';
			mul_rdy_ff <= mul_rdy_ff(0) & mul_rdy;
			case state is
				when IDLE =>
					valid_strob_o <= '0';
					cnt <= 0;
					if start_i = '1' then
						state <= INIT;
						alpha <= alpha_i;
					end if;
				when INIT =>
					alpha_plus_one <= resize(alpha + to_sfixed(1.0, INT-1, -FRC), INT-1, -FRC);
					state <= STEP1;
					first_val_sent <= '0';
					weight <= to_sfixed(1.0, INT-1, -FRC);
				when STEP1 =>
					-- prepare operands and init sync to multiplier
					operand1 <= alpha_plus_one;
					operand2 <= inversely_proportional_coefficients(cnt);
					mul_sync <= '1';
					state <= STEP1_WAIT;
				when STEP1_WAIT =>
					if mul_rdy_ff(0) = '1' then
						alpha_plus_one_div_j <= resize(to_sfixed(mul_result, 2*INT-1, -2*FRC), INT-1, -FRC);
						state <= STEP2;
					end if;
				when STEP2 =>
					full_coef <= resize(to_sfixed(1.0, INT-1, -FRC) - alpha_plus_one_div_j, INT-1, -FRC);
					state <= STEP3;
				when STEP3 =>
					-- prepare operands and init sync to multiplier, assert valid strobe - sinse this moment the valid outputs will be on weight_stream_o
					valid_strob_o <= '1';
					ready_o <= not first_val_sent; -- the 1st ready for the W[0] = 1
					first_val_sent <= '1';
					operand1 <= weight;
					operand2 <= full_coef;
					mul_sync <= '1';
					state <= STEP3_WAIT;
				when STEP3_WAIT =>
					if mul_rdy_ff(0) = '1' then -- when the mul result is ready - output the result to weight_stream_o and generate ready_o = 1
						ready_o <= '1';
						cnt <= cnt + 1;
						weight <= resize(to_sfixed(mul_result, 2*INT-1, -2*FRC), INT-1, -FRC);
						if cnt = N-2 then -- when the last weight is output. N-2 - is because the 1st W[0] is outputted uncounted
							state <= IDLE;
						else
							state <= STEP1;
						end if;
					end if;
				when others => state <= IDLE;
			end case;

			if clr_i = '1' then
				state <= IDLE;
			end if;
		end if;
	end process;

	weight_stream_o <= weight;

end;