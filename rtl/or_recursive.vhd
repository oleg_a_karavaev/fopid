library ieee;
use ieee.std_logic_1164.all;

-- The recursive OR function that folds the array by the OR function as the tree with Node and branches
entity or_recursive is
generic (
	  order : integer := 1 -- the current order of the "tree" - actually the width of the input data
);
port (
	  a : in std_logic_vector(order-1 downto 0)
	; z : out std_logic
);
end entity or_recursive;

architecture rtl of or_recursive is

	component or_recursive is -- use it recursivelly
	generic (
		  order : integer := 1
	);
	port (
		  a : in std_logic_vector(order-1 downto 0)
		; z : out std_logic
	);
	end component;

	constant node : integer := order / 2; 

begin

	singleton_gen: if order = 1 generate -- if the input is singletone array (i.e. 1 bit) the outpu is the input
		z <= a(0);
	end generate;

	pair_gen: if order = 2 generate -- when there is 2 inputs - the output is the OR of these inputs
		z <= a(0) or a(1);
	end generate;

	multiple_gen: if order > 2 generate -- If the input width is bigger then 2
		signal results : std_logic_vector(1 downto 0);
	begin
		-- the left branch
		left: or_recursive
		generic map(
			  order => order / 2
		)
		port map(
			  a => a(order-1 downto order - order / 2)
			, z => results(0)
		);

		-- the right branch - the rest
		right: or_recursive
		generic map(
			  order => (order - order / 2)
		)
		port map(
			  a => a(order-order/2-1 downto 0)
			, z => results(1)
		);

		z <= results(0) or results(1); -- The Node and the output
	end generate;
end;