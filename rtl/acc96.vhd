-- 96-Bit Accumulator, Base on ug193.pdf page 77

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;


entity acc96 is
port(
	  clk_i: in std_logic
	; clr_i : in std_logic
	; data_i: in std_logic_vector(73 downto 0)
	; sync_i: in std_logic
	; subtract : in std_logic
	; acc_o: out std_logic_vector(95 downto 0)
	; sync_o: out std_logic
);
end entity;

architecture rtl of acc96 is
	
	component DSP48E is
	generic (
		  ACASCREG : integer :=  1      
		; ALUMODEREG : integer :=  1     
		; AREG : integer :=  1
		; AUTORESET_PATTERN_DETECT : string :=  "FALSE"
		; AUTORESET_PATTERN_DETECT_OPTINV : string :=  "MATCH"
		; A_INPUT : string :=  "DIRECT"
		; BCASCREG : integer :=  1
		; BREG : integer :=  1
		; B_INPUT : string :=  "DIRECT"
		; CARRYINREG : integer :=  1
		; CARRYINSELREG : integer := 1
		; CREG : integer := 1
		; MASK : bit_vector := X"3FFFFFFFFFFF"
		; MREG : integer := 1
		; MULTCARRYINREG : integer :=  1
		; OPMODEREG : integer := 1
		; PATTERN : bit_vector := X"000000000000"
		; PREG : integer := 1
		; SEL_MASK : string :=  "MASK"
		; SEL_PATTERN : string :=  "PATTERN"
		; SEL_ROUNDING_MASK : string :=  "SEL_MASK"
		; USE_MULT : string :=  "MULT_S"
		; USE_PATTERN_DETECT : string :=  "NO_PATDET"
		; USE_SIMD : string :=  "ONE48"
	);
	port (
		  A : in std_logic_vector(29 downto 0)
		; B : in std_logic_vector(17 downto 0)
		; C : in std_logic_vector(47 downto 0)
		; OPMODE : in std_logic_vector(6 downto 0)
		; ALUMODE : in std_logic_vector(3 downto 0)
		; CARRYIN : in std_logic
		; CARRYINSEL : in std_logic_vector(2 downto 0)
		; CEA1 : in std_logic
		; CEA2 : in std_logic
		; CEB1 : in std_logic
		; CEB2 : in std_logic
		; CEC : in std_logic
		; CEM : in std_logic
		; CEP : in std_logic
		; CEALUMODE : in std_logic
		; CECTRL : in std_logic
		; CEMULTCARRYIN : in std_logic
		; CECARRYIN : in std_logic
		; RSTA : in std_logic
		; RSTB : in std_logic
		; RSTC : in std_logic
		; RSTM : in std_logic
		; RSTP : in std_logic
		; RSTCTRL : in std_logic
		; RSTALLCARRYIN : in std_logic
		; CLK : in std_logic
		; RSTALUMODE : in std_logic
		; ACIN : in std_logic_vector(29 downto 0)
		; BCIN : in std_logic_vector(17 downto 0)
		; PCIN : in std_logic_vector(47 downto 0)
		; CARRYCASCIN : in std_logic
		; MULTSIGNIN : in std_logic
		; ACOUT : out std_logic_vector(29 downto 0)
		; BCOUT : out std_logic_vector(17 downto 0)
		; PCOUT : out std_logic_vector(47 downto 0)
		; P : out std_logic_vector(47 downto 0)
		; CARRYOUT : out std_logic_vector(3 downto 0)
		; CARRYCASCOUT : out std_logic
		; MULTSIGNOUT : out std_logic
		; PATTERNDETECT : out std_logic
		; PATTERNBDETECT : out std_logic
		; OVERFLOW : out std_logic
		; UNDERFLOW : out std_logic
	);
	end component;

	function replicate (input : in std_logic; size : in positive) return std_logic_vector is
		variable r : std_logic_vector(size-1 downto 0);
	begin
		for i in r'range loop
			r(i) := input;
		end loop;
		return r;
	end function;

	signal CARRYCASCOUT : std_logic;
	signal sync_delay_line : std_logic_vector(2 downto 0) := "000";

	signal DSPH_A : std_logic_vector(29 downto 0);
	signal DSPH_B : std_logic_vector(17 downto 0);
	signal DSP_ALUMODE : std_logic_vector(3 downto 0);
	constant ZERO30 : std_logic_vector(29 downto 0) := (others=> '0');
	constant ZERO48 : std_logic_vector(47 downto 0) := (others=> '0');
	constant ZERO18 : std_logic_vector(17 downto 0) := (others=> '0');
begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			sync_delay_line <= sync_delay_line(1 downto 0) & sync_i;
			--if clr_i = '1' then
			--	sync_delay_line <= (others => '0');
			--end if;
		end if;
	end process;

	sync_o <= sync_delay_line(2);

	DSPH_A <= replicate(data_i(73), 22) & data_i(73 downto 66);
	DSPH_B <= data_i(65 downto 48);
	DSP_ALUMODE <= "00" & subtract & subtract;
	
	DSP48E_H : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => "FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 1       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 1
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 0
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "NONE"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSPH_A
		, B => DSPH_B
		, C => ZERO48
		, OPMODE => "0100011"
		, ALUMODE => DSP_ALUMODE
		, CARRYIN => '0'
		, CARRYINSEL => "010"
		, CEA1 => '1' --sync_i
		, CEA2 => '1' --sync_delay_line(0)
		, CEB1 => '1' --sync_i
		, CEB2 => '1' --sync_delay_line(0)
		, CEC => '0'
		, CEM => '1'
		, CEP => sync_delay_line(1)
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => sync_delay_line(0)
		, RSTA => clr_i
		, RSTB => clr_i
		, RSTC => clr_i
		, RSTM => clr_i
		, RSTP => clr_i
		, RSTCTRL => clr_i
		, RSTALLCARRYIN => clr_i
		, CLK => clk_i
		, RSTALUMODE => clr_i
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => ZERO48
		, CARRYCASCIN => CARRYCASCOUT
		, MULTSIGNIN => '0'
		, ACOUT => open
		, BCOUT => open
		, PCOUT => open
		, P => acc_o(95 downto 48)
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP48E_L : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 1
		, AUTORESET_PATTERN_DETECT => "FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 1       
		, BREG => 1
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 1
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 0
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "NONE"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => ZERO30
		, B => ZERO18
		, C => data_i(47 downto 0)
		, OPMODE => "0101100"
		, ALUMODE => DSP_ALUMODE
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => sync_i
		, CEM => '1'
		, CEP => sync_delay_line(0)
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '0'
		, RSTA => clr_i
		, RSTB => clr_i
		, RSTC => clr_i
		, RSTM => clr_i
		, RSTP => clr_i
		, RSTCTRL => clr_i
		, RSTALLCARRYIN => clr_i
		, CLK => clk_i
		, RSTALUMODE => clr_i
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => ZERO48
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => open
		, BCOUT => open
		, PCOUT => open
		, P => acc_o(47 downto 0)
		, CARRYOUT => open
		, CARRYCASCOUT => CARRYCASCOUT
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

end architecture;