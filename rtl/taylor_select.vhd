-- Taylor series implementation for either a natural logarithm or an exponential function
-- latency 28+26*N clocks, where N - is a number of elements in the Taylor series
-- accuracy for the natural logarithm less then ~10^(-5), for the exponential function 0.001 for arguments near 2.5, and ~10(^-6) - 10(^-8) while moving left
-- the all accuracy are about a values range in regarding with current task
-- Author: Oleg Karavaev

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;
use std.textio.all; 

entity taylor_select is
generic (
	  IN_INT :integer :=8
	; IN_FRC :integer :=24
	; OUT_INT:integer :=8
	; OUT_FRC:integer :=24
	; LN_ORDER : integer := 64  -- the number of Taylor series's elements for the Natural logarithm (so lot is because of small arguments near 0)
	; EXP_ORDER : integer := 12  -- the number of Taylor series's elements for the exponentiation function
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic                                -- synchronous clear/reset
	; start_i : in std_logic                              -- this pulse need to start the computation
	; ready_o : out std_logic                             -- this output pulse generated when the computation is ready
	; argument_i : in sfixed(IN_INT-1 downto -IN_FRC)
	; operation_i : in std_logic                          -- operation select: 0 - ln(a), 1 - exp(a)
	; result_o : out sfixed(OUT_INT-1 downto -OUT_FRC)
);
end taylor_select;

architecture rtl of taylor_select is

	-- A main idea.
	-- 1. multiplying the argument by it self: x, x^2, x^3, x^4
	-- 2. multiplying the current power of the argument by the coeficient from the series: 1, 1/2, 1/3, 1/4 for ln(x), and 1, 1/2!, 1/3!, 1/4!, 1/5!, ... for exp(x)
	-- 3. sign control during accumulation:
	--       ln(1+x) = x - x^2/2 + x^3/3 - x^4/4 + x^5/5 ...
	--       exp(x) = 1 + x + x^2/2! + x^3/3! + x^4/4! + ...
	-- 4. When the defined number of element are processed - output the result anf the ready pulse

	-- There is a problem with exponential function. Because of the 1st opeartion is power of arg and this is continuously it is need to store up to x^(EXP_ORDER) values
	-- To do this need the wide INT part of a fixed point value. And this limits the bit size of a fractional part. A next problem is that the coefficients are an inversion of factorial function
	-- so it is need a wide fractional part to use a big number of elements. But this fractional part is limited by the previous problem.
	-- These problems are the cause of a "less" accuracy on the right bound of the values of the current task.

	constant INT_EXT : integer := 10;

	-- the wide range of fixed point for internal operations.
	-- This is because of fixed point. It is need to do it wider to increase the accuracy.
	-- floating point will be more complicated in implementation but with better accuracy.
	constant MUL_INT : integer := 18;
	constant MUL_FRC : integer := 41;
	
	signal argument : std_logic_vector(MUL_INT + MUL_FRC - 1 downto 0) := (others => '0');
	signal job : std_logic := '0';
	signal step, step_last : integer := 0;
	signal step_limit : integer := 16;
	signal pow_x_net : std_logic_vector(117 downto 0);
	signal pow_x_ff : sfixed(MUL_INT-1 downto -MUL_FRC);
	signal taylor_coef : sfixed(MUL_INT-1 downto -MUL_FRC);
	signal intermediate_result_full : std_logic_vector(117 downto 0);
	signal intermediate_result_full_sfixed : sfixed(2*IN_INT+INT_EXT-1 downto -2*IN_FRC);
	signal intermediate_result_fit_sfixed : sfixed(IN_INT+INT_EXT-1 downto -IN_FRC);


	type sfixed_vector is array (integer range <>) of sfixed(MUL_INT-1 downto -MUL_FRC);
	-- coefficients fot Ln(x)
	constant ln_constants : sfixed_vector(0 to 100) := (
		  to_sfixed(1.0, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/2, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/3, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/4, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/5, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/6, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/7, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/8, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/9, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/10,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/11,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/12,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/13,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/14,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/15,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/16,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/17,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/18, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/19, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/20, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/21, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/22, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/23, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/24, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/25, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/26,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/27,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/28,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/29,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/30,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/31,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/32,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/33,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/34, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/35, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/36, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/37, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/38, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/39, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/40, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/41, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/42,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/43,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/44,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/45,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/46,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/47, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/48, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/49, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/50, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/51, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/52, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/53, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/54, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/55,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/56,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/57,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/58,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/59,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/60,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/61,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/62,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/63, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/64, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/65, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/66, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/67, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/68, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/69, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/70, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/71,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/72,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/73,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/74,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/75,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/76,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/77,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/78,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/79, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/80, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/81, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/82, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/83, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/84, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/85, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/86, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/87,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/88,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/89,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/90,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/91,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/92,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/93,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/94,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/95,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/96,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/97,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/98,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/99,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/100,MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/101,MUL_INT-1,-MUL_FRC)
	);

	-- factorial function. This is need for ability of dual functionality
	function factorial (constant order : in integer) return real is
		variable v : integer := 1;
		variable r : integer := 1;
	begin
		while (v <= order) loop
			r := r * v;
			v := v + 1;
		end loop;
		return real(r);
	end function;
	-- exp coefficients
	constant exp_constants : sfixed_vector(0 to 100) := (
		  to_sfixed(1.0,             MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(2),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(3),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(4),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(5),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(6),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(7),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(8),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(9),  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(10), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(11), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(12), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(13), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(14), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(15), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(16), MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/factorial(17), MUL_INT-1,-MUL_FRC)
		, others => to_sfixed(0.0, MUL_INT-1,-MUL_FRC)
	);

	signal taylor_constants : sfixed_vector(0 to 100) := ln_constants;
	-- elements sign control constants
	constant ln_constants_sign : std_logic_vector(0 to 100) := "01010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010";
	constant exp_constants_sign : std_logic_vector(0 to 100) := "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
	signal taylor_constants_sign : std_logic_vector(0 to 100) := ln_constants_sign;



	component mult42x32 is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(41 downto 0)
		; data1_i: in std_logic_vector(31 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(73 downto 0)
		; sync_o: out std_logic
	);
	end component mult42x32;

	component mult59x59 is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(58 downto 0)
		; data1_i: in std_logic_vector(58 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(117 downto 0)
		; sync_o: out std_logic
	);
	end component;

	component acc96 is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data_i: in std_logic_vector(73 downto 0)
		; sync_i: in std_logic
		; subtract : in std_logic
		; acc_o: out std_logic_vector(95 downto 0)
		; sync_o: out std_logic
	);
	end component acc96;

	signal pow_x_rdy : std_logic;
	signal taylor_acc_sync : std_logic;
	signal taylor_series : std_logic_vector(95 downto 0);
	signal coef_rdy, acc_rdy, pow_x_sync, coef_sync : std_logic := '0';
	signal cnt_en : std_logic := '0';
	signal start_ff : std_logic_vector(1 downto 0) := "00";
	signal job_pending : std_logic := '0';
	signal acc_inited : std_logic := '0';
begin
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			-- the all these signal are pulses so must be deasserted at ech clock cycle
			pow_x_sync <= '0';
			taylor_acc_sync <= '0';
			ready_o <= '0';
			---------------------------------------------------------------------------
			start_ff <= start_ff(0) & start_i;
			if start_i = '1' and job_pending = '0' then -- start the computation
				argument <= to_slv(resize(argument_i, MUL_INT-1, -MUL_FRC));
				pow_x_ff <= to_sfixed(1.0, MUL_INT-1, -MUL_FRC); -- initial value of argument power multiplier. Must be 1 to got the first elements X (1*x = x - the 1st element of the series)
				job_pending <= '1'; -- need some pause to initialize multipliers after reset
				step <= 0;
				cnt_en <= '0';
				acc_inited <= '0';
				step_last <= 0;
				--taylor_acc_sync <= '1';
				-- this 1st element will go 1st into the accumulator (acc96)
				if operation_i = '0' then -- in the ln the 1st element is x or in other words 0 + x (in unification sense)
					intermediate_result_full_sfixed <= to_sfixed(0.0, 2*IN_INT+INT_EXT-1, -2*IN_FRC);
				else                      -- in the exp the 1st element is 1 or in other words 1 + x
					intermediate_result_full_sfixed <= to_sfixed(1.0, 2*IN_INT+INT_EXT-1, -2*IN_FRC);
				end if;
			end if;

			if job_pending = '1' then -- multipliers init done, so start to multiply
				if start_ff(1) = '1' then
					job <= '1';
					pow_x_sync <= '1';
				end if;
			end if;

			if job = '1' then

				if step_last <= step_limit then
					taylor_acc_sync <= coef_rdy;					
				end if;
				if acc_rdy = '1' and step_last < step_limit and cnt_en = '1' then -- this counter is used for sign control in the acc
					step_last <= step_last + 1;
				end if;
				if coef_rdy = '1' and step < step_limit then -- this counter is used for coef control in the coef mul
					step <= step + 1;
				end if;
				coef_sync <= pow_x_rdy;
				if coef_rdy = '1' then -- fix data on each coef mul ready to send it into the acc
					intermediate_result_full_sfixed <= resize(to_sfixed(intermediate_result_full, 2*MUL_INT-1, -2*MUL_FRC), 2*IN_INT+INT_EXT-1, -2*IN_FRC);
				end if;
				if pow_x_rdy = '1' then -- fix data on each arg mul (power) ready to send it into the coef mul
					cnt_en <= '1';
					pow_x_ff <= resize(to_sfixed(pow_x_net, 2*MUL_INT-1, -2*MUL_FRC), MUL_INT-1, -MUL_FRC);
					pow_x_sync <= '1';
				end if;
				if acc_rdy = '1' then -- acc single operation complete
					result_o <= resize(to_sfixed(taylor_series(63 downto 0), 2*OUT_INT-1, -2*OUT_FRC), OUT_INT-1, -OUT_FRC);
					if step_last = step_limit then -- if this operation is last - the ready is asserted
						ready_o <= '1';
						job <= '0';
						job_pending <= '0';
					end if;
				end if;

				if acc_inited = '0' then -- the first (initial) write into the account after computation have beginning
					if coef_sync = '1' then
						acc_inited <= '1';
						taylor_acc_sync <= '1';
					end if;
				end if;

			end if;
		end if;
	end process;
	
	-- select elements (or steps) limits, sign constants and coef constants in regarding to the operation
	step_limit <= LN_ORDER when operation_i = '0' else EXP_ORDER;
	taylor_constants <= ln_constants when operation_i = '0' else exp_constants;
	taylor_constants_sign <= ln_constants_sign when operation_i = '0' else exp_constants_sign;

	-- get the current coef
	taylor_coef <= taylor_constants(step);

	-- powers of x
	pow_x : mult59x59
	port map (
		  clk_i => clk_i
		, clr_i => clr_i or start_i
		, data0_i => to_slv(pow_x_ff)
		, data1_i => argument
		, sync_i => pow_x_sync
		, data_o => pow_x_net
		, sync_o => pow_x_rdy
	);

	-- coef
	coef : mult59x59
	port map(
		  clk_i => clk_i
		, clr_i => clr_i or start_i
		, data0_i => to_slv(pow_x_ff)
		, data1_i => to_slv(taylor_coef)
		, sync_i => coef_sync
		, data_o => intermediate_result_full
		, sync_o => coef_rdy
	);

	-- the accumulator - there the value of the Taylor series is collected
	taylor_acc : acc96
	port map (
		  clk_i => clk_i
		, clr_i => clr_i or start_i
		, data_i => to_slv(intermediate_result_full_sfixed)
		, sync_i => taylor_acc_sync
		, subtract => taylor_constants_sign(step_last)
		, acc_o => taylor_series
		, sync_o => acc_rdy
	);

end;

-- Taylor series implementation for an exponentiation function
-- latency 15+13*N clocks, where N - is a number of elements in the Taylor series
-- accuracy ~10(^-5) (worse case ~2.5), 10(^-8) while moving left
-- the all accuracy are about a values range in regarding with current task
-- Author: Oleg Karavaev

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;
use std.textio.all; 

entity taylor_exp is
generic (
	  IN_INT :integer :=8
	; IN_FRC :integer :=24
	; OUT_INT:integer :=8
	; OUT_FRC:integer :=24
	; step_limit : integer := 32
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic
	; start_i : in std_logic
	; ready_o : out std_logic
	; argument_i : in sfixed(IN_INT-1 downto -IN_FRC)
	; result_o : out sfixed(OUT_INT-1 downto -OUT_FRC)
);
end taylor_exp;

architecture rtl of taylor_exp is
	
	-- A main idea
	-- get an argument, multiply it by coef, multiply it by arg, then to multiply the result by the next coef and go on.
	-- exp(x) = 1 + x + x^2/2! + x^3/3! + ... = 1 + x + (x/2)*x + (((x/2)*x)/3)*x + ((((x/2)*x)/3)*x)/4*x + ...
	-- This method fixes the problems of the previous realisation.
	-- There is no any high powers at each step. An each power of x is compensated by reducing the next coef with the factorial function's speed
	-- So it is valid to have a small INT part of  a fixed point value and to increase the size of fractional part
	-- And the accuracy is increased by increasing a fractional part

	constant INT_EXT : integer := 10;

	constant MUL_INT : integer := 8;
	constant MUL_FRC : integer := 51;
	
	signal argument : std_logic_vector(MUL_INT + MUL_FRC - 1 downto 0) := (others => '0');
	signal job : std_logic := '0';
	signal step, step_last : integer := 0;
	signal pow_x_net, coef_result, coef_result_ff : std_logic_vector(117 downto 0);
	signal pow_x_ff, coef_result_resized : sfixed(MUL_INT-1 downto -MUL_FRC);
	signal taylor_coef : sfixed(MUL_INT-1 downto -MUL_FRC);
	signal intermediate_result_full, intermediate_result_full_ff : std_logic_vector(117 downto 0);
	signal intermediate_result_full_sfixed, intermediate_result_full_resized : sfixed(2*IN_INT+INT_EXT-1 downto -2*IN_FRC);
	signal intermediate_result_fit_sfixed : sfixed(IN_INT+INT_EXT-1 downto -IN_FRC);

	signal taylor_series_resized : sfixed(OUT_INT-1 downto -OUT_FRC);

	type sfixed_vector is array (integer range <>) of sfixed(MUL_INT-1 downto -MUL_FRC);
	-- coef multiplicands
	constant exp_constants : sfixed_vector(0 to 100) := (
		  to_sfixed(1.0,    MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/2,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/3,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/4,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/5,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/6,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/7,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/8,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/9,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/10, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/11, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/12, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/13, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/14, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/15, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/16, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/17, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/18,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/19,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/20,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/21,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/22,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/23,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/24,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/25,  MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/26, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/27, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/28, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/29, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/30, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/31, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/32, MUL_INT-1,-MUL_FRC)
		, to_sfixed(1.0/33, MUL_INT-1,-MUL_FRC)
		, others => to_sfixed(0.0, MUL_INT-1,-MUL_FRC)
	);
	-- sign constants - just a heritage from the previous realisation
	constant exp_constants_sign : std_logic_vector(0 to 100) := "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

	component mult59x59 is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(58 downto 0)
		; data1_i: in std_logic_vector(58 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(117 downto 0)
		; sync_o: out std_logic
	);
	end component;

	component acc96 is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data_i: in std_logic_vector(73 downto 0)
		; sync_i: in std_logic
		; subtract : in std_logic
		; acc_o: out std_logic_vector(95 downto 0)
		; sync_o: out std_logic
	);
	end component acc96;

	signal pow_x_rdy : std_logic;
	signal taylor_acc_sync : std_logic;
	signal taylor_series, taylor_series_ff : std_logic_vector(95 downto 0);
	signal coef_rdy, acc_rdy, pow_x_sync, coef_sync : std_logic := '0';
	signal cnt_en : std_logic := '0';
	signal start_ff : std_logic_vector(1 downto 0) := "00";
	signal job_pending : std_logic := '0';
	signal acc_inited : std_logic := '0';

	-- FSM of computations queue
	type state_t is (IDLE, INIT, MUL_TO_COEF, MUL_TO_ARG, LAST_TO_ACC);
	signal state : state_t := IDLE;

	signal coef_rdy_ff : std_logic_vector(2 downto 0) := "000";
	signal pow_x_rdy_ff : std_logic_vector(1 downto 0) := "00";
	signal acc_rdy_ff : std_logic_vector(1 downto 0) := "00";
begin
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			pow_x_sync <= '0';
			coef_sync <= '0';
			taylor_acc_sync <= '0';
			ready_o <= '0';
			start_ff <= start_ff(0) & start_i;
			coef_rdy_ff <= coef_rdy_ff(1 downto 0) & coef_rdy;
			coef_result_resized <= resize(to_sfixed(coef_result, 2*MUL_INT-1, -2*MUL_FRC), MUL_INT-1, -MUL_FRC);
			intermediate_result_full_resized <= resize(to_sfixed(intermediate_result_full, 2*MUL_INT-1, -2*MUL_FRC), 2*IN_INT+INT_EXT-1, -2*IN_FRC);
--			intermediate_result_full_ff <= intermediate_result_full;
--			coef_result_ff <= coef_result;
			pow_x_rdy_ff <= pow_x_rdy_ff(0) & pow_x_rdy;
--			taylor_series_ff <= taylor_series;
			taylor_series_resized <= resize(to_sfixed(taylor_series(63 downto 0), 2*OUT_INT-1, -2*OUT_FRC), OUT_INT-1, -OUT_FRC);
			acc_rdy_ff <= acc_rdy_ff(0) & acc_rdy;
			case state is
				when IDLE =>
					if start_i = '1' then	-- start, reset acc, go to 1st (after reset) initialisation of multipliers and acc, set an initial values
						state <= INIT;
						argument <= to_slv(resize(argument_i, MUL_INT-1, -MUL_FRC));
						pow_x_ff <= to_sfixed(1.0, MUL_INT-1, -MUL_FRC);
						step <= 0;
						cnt_en <= '0';
						step_last <= 0;
						intermediate_result_full_sfixed <= to_sfixed(1.0, 2*IN_INT+INT_EXT-1, -2*IN_FRC); -- the 1st elements of the exp Taylor series is 1
					end if;
				when INIT =>
					if start_ff(1) = '1' then -- go to ceof mul stage
						coef_sync <= '1';
						state <= MUL_TO_COEF;
						taylor_acc_sync <= '1';
					end if;
				when MUL_TO_COEF =>
					if coef_rdy_ff(2) = '1' then -- mul curent variable state by coef
						cnt_en <= '1';
						pow_x_ff <= coef_result_resized;
						state <= MUL_TO_ARG;
						pow_x_sync <= '1';
					end if;
				when MUL_TO_ARG =>
					if pow_x_rdy_ff(1) = '1' then -- mul the current variable state by argument to get the next element value and send it into the ACC
						intermediate_result_full_sfixed <= intermediate_result_full_resized;
						taylor_acc_sync <= '1';
						step <= step + 1;
						if step = step_limit then -- if last value - go to last stage
							state <= LAST_TO_ACC;
						else                      -- else repeat computation queue
							state <= MUL_TO_COEF;
							coef_sync <= '1';
						end if;
					end if;
				when LAST_TO_ACC =>
					if acc_rdy_ff(1) = '1' then -- outputs the result
						result_o <= taylor_series_resized;-- resize(to_sfixed(taylor_series_ff(63 downto 0), 2*OUT_INT-1, -2*OUT_FRC), OUT_INT-1, -OUT_FRC);
						ready_o <= '1';
						state <= IDLE;
					end if;
				when others => state <= IDLE;
			end case;
			if acc_rdy = '1' then
				step_last <= step_last + 1;
			end if;
		end if;
	end process;

	taylor_coef <= exp_constants(step);

	-- powers of x
	pow_x : mult59x59
	port map (
		  clk_i => clk_i
		, clr_i => clr_i or start_i
		, data0_i => to_slv(pow_x_ff)
		, data1_i => argument
		, sync_i => pow_x_sync
		, data_o => intermediate_result_full
		, sync_o => pow_x_rdy
	);

	-- coef
	coef : mult59x59
	port map(
		  clk_i => clk_i
		, clr_i => clr_i or start_i
		, data0_i => to_slv(resize(intermediate_result_full_sfixed, MUL_INT-1, -MUL_FRC))
		, data1_i => to_slv(taylor_coef)
		, sync_i => coef_sync
		, data_o => coef_result
		, sync_o => coef_rdy
	);

	taylor_acc : acc96
	port map (
		  clk_i => clk_i
		, clr_i => clr_i or start_i
		, data_i => to_slv(intermediate_result_full_sfixed)
		, sync_i => taylor_acc_sync
		, subtract => exp_constants_sign(step_last)
		, acc_o => taylor_series
		, sync_o => acc_rdy
	);

end;