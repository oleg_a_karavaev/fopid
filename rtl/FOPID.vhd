-- The FOPID module
-- 3 parts: linear path (Kp*X), integral part (Ki*1/S^(lambda)), derivative part (Kd*S^(mu))
-- the main idea:
-- at first need to load the weights
-- then to pass the data sample one by one sync'ed by the validation strobe. These sample will go to the data delay buffer which is to orginize a FIR-like delay line
-- When the start input is received it starts the processing in the both integral and derivative parts
-- The data samples from the data delay buffer and the weights from the weights containers is passed synchronously to the computation core of the GL modules
-- After the all multiplications are computed and accumulated for the current data delay bufer state
-- the result of the GL modules are multiplied by the K-coefficients and added together

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

entity FOPID is
generic (
	  N : integer := 64    -- Window Size == Weights number
	; INT : integer := 8   -- Int part size of a fixed point
	; FRC : integer := 24  -- frac part size of a fixed point
	; family : string := "spartan6"
);
port (
	  clk_i : in std_logic                                    -- clock
	; clr_i : in std_logic                                    -- synchronous reset
	; data_pscl_i : in std_logic                              -- input sample prescaler signal or validation pulse that is the same
	; C0 : in sfixed(INT-1 downto -FRC)                       -- the C0 linearization constant
	; C1 : in sfixed(INT-1 downto -FRC)                       -- the C1 linearization constant
	; H : in sfixed(INT-1 downto -FRC)                        -- the FOPID Step
	; start_i : in std_logic                                  -- start pulse - is to start the FOPID algorithm with the current state of data registers and delay line and the weights containers
	; ready_to_receive_data_o : out std_logic                 -- is available to receive a next data, i.e. not in the processing state
	; data_i : in sfixed(INT-1 downto -FRC)                   -- the input data samples which are sync'ed by the data_pscl_i
	; weight_pscl_i : in std_logic                            -- sync pulses for the weights loading
	; integral_weight_i : in sfixed(INT-1 downto -FRC)        -- the weights for the integral GL-component
	; integral_weights_control_i : in std_logic               -- the integral weights validation strobe
	; derivative_weight_i : in sfixed(INT-1 downto -FRC)      -- the weights for the derivative GL-component
	; derivative_weights_control_i : in std_logic             -- the derivative weigths validation strobe
	; derivative_weights_containers_full_o : out std_logic    -- the derivative weights container is full
	; intergal_weights_containers_full_o : out std_logic      -- the integral weights container is full
	; data_control_full_o : out std_logic                     -- the data delay buffer is full (just in case)
	; force_i : in std_logic                                  -- 1 - ignore the weights container's state and the partially filled data delay buffers
	; valid_o : out std_logic                                 -- the result validation signal. the each pulse for the each start
	; data_o : out sfixed(INT-1 downto -FRC)                  -- the FOPID result
	; Kp_i : in sfixed(INT-1 downto -FRC)                     -- the linear path coeff
	; Ki_i : in sfixed(INT-1 downto -FRC)                     -- the integral path coef
	; Kd_i : in sfixed(INT-1 downto -FRC)                     -- the derivative path coef
);
end entity FOPID;

architecture rtl of FOPID is

	function GetINT(family : in string) return integer is
	begin
		if family = "spartan6" then return 11;
		else return 11;
		end if;
	end function GetINT;

	function GetFRC(family : in string) return integer is
	begin
		if family = "spartan6" then return 24;
		else return 24;
		end if;
	end function GetFRC;

	constant INT_MUL : integer := GetINT(family);
	constant FRC_MUL : integer := GetFRC(family);

	component GL_core is
	generic (
		  N : integer := 64
		; INT : integer := 8
		; FRC : integer := 24
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; C0 : in sfixed(INT-1 downto -FRC)
		; C1 : in sfixed(INT-1 downto -FRC)
		; H : in sfixed(INT-1 downto -FRC)
		; data_pscl_i : in std_logic
		; start_i : in std_logic
		; ready_to_receive_data_o : out std_logic -- is available to receive a next data
		; data_control_full_o : out std_logic
		; data_i : in sfixed(INT-1 downto -FRC)
		; weight_pscl_i : in std_logic
		; weight_i : in sfixed(INT-1 downto -FRC)
		; weights_containers_full_o : out std_logic
		; force_i : in std_logic -- 1 - ignore weights container's state
		; valid_o : out std_logic -- data valid strobe
		; data_o : out sfixed(2*INT-1 downto -2*FRC)
	);
	end component GL_core;

	component mult59x59_pipe is
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(58 downto 0)
		; data1_i: in std_logic_vector(58 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(117 downto 0)
		; sync_o: out std_logic
	);
	end component mult59x59_pipe;

	component mult35x35_pipe is -- DSP-slice based multiplier 35x35
	generic (
		  family : string := "spartan6"
	);
	port (
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(34 downto 0)
		; data1_i: in std_logic_vector(34 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(69 downto 0)
		; sync_o: out std_logic
	);
	end component mult35x35_pipe;

	signal integral_data_control_full, derivative_data_control_full : std_logic;
	signal K_common_mul_sync_result_sync_vector : std_logic_vector(2 downto 0);
	signal K_common_mul_sync : std_logic;
	signal fetch : std_logic_vector(2 downto 0);
	signal K, data : sfixed(INT_MUL-1 downto -FRC_MUL);
	signal integral_valid, derivative_valid : std_logic;
	signal integral, derivative, integral_fetched, derivative_fetched, result_acc : sfixed(2*INT-1 downto -2*FRC);
	signal K_common_mul_sync_result : std_logic_vector(2*(INT_MUL+FRC_MUL)-1 downto 0);

	type sfixed_vector is array (integer range <>) of sfixed(INT-1 downto -FRC);
	signal data_delay_line : sfixed_vector(N-1 downto 0);
	signal integral_weights_containers_full, derivative_weights_containers_full : std_logic;
	signal integral_ready_to_receive_data, derivative_ready_to_receive_data, K_common_mul_sync_result_sync : std_logic;
--	signal K_real, data_real : real;
begin

	data_control_full_o <= integral_data_control_full;
	intergal_weights_containers_full_o <= integral_weights_containers_full;
	derivative_weights_containers_full_o <= derivative_weights_containers_full;
	ready_to_receive_data_o <= integral_ready_to_receive_data;

	-- integral GL component
	GL_integral : GL_core
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family =>family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data_pscl_i => data_pscl_i
		, C0 => C0
		, C1 => C1
		, H => H
		, start_i => start_i
		, ready_to_receive_data_o => integral_ready_to_receive_data
		, data_control_full_o => integral_data_control_full --integral_data_control_empty
		, data_i => data_i
		, weight_pscl_i => weight_pscl_i and integral_weights_control_i
		, weight_i => integral_weight_i
		, weights_containers_full_o => integral_weights_containers_full
		, force_i => '0'
		, valid_o => integral_valid
		, data_o => integral
	);

	-- derivative GL-component
	GL_derivative : GL_core
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data_pscl_i => data_pscl_i
		, C0 => C0
		, C1 => C1
		, H => H
		, start_i => start_i
		, ready_to_receive_data_o => derivative_ready_to_receive_data
		, data_control_full_o => derivative_data_control_full --derivative_data_control_empty
		, data_i => data_i
		, weight_pscl_i => weight_pscl_i and derivative_weights_control_i
		, weight_i => derivative_weight_i
		, weights_containers_full_o => derivative_weights_containers_full
		, force_i => '0'
		, valid_o => derivative_valid
		, data_o => derivative
	);

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			-- when the derivative part is ready
			if derivative_valid = '1' then
				derivative_fetched <= derivative; -- fix the derivative result
				fetch(2) <= '1';                  -- derivative ready flag
			end if;
			-- when the integral part is complete
			if integral_valid = '1' then
				integral_fetched <= integral;     -- fix the integral result
				fetch(1) <= '1';                  -- integral ready flag
				fetch(0) <= '1';                  -- lineear ready flag - it will be already ready at this time so it is valid to do so
			end if;
			-- data sample delay line to synchronize the input sample with the GL's data delay buffers size. This need only when force_i = 0
			if data_pscl_i = '1' then
				data_delay_line <= data_delay_line(data_delay_line'left-1 downto 0) & data_i;
			end if;
			-- at first when the derivative is ready - pass it's data to the common output multiplier
			if fetch(2) = '1' then
				K <= resize(Kd_i, INT_MUL-1, -FRC_MUL);
				data <= resize(derivative_fetched, INT_MUL-1, -FRC_MUL);
				fetch(2) <= '0'; -- reset the ready flag
			-- then when the integral is ready - pass it's data to the common output multiplier
			elsif fetch(1) = '1' then
				K <= resize(Ki_i, INT_MUL-1, -FRC_MUL);
				data <= resize(integral_fetched, INT_MUL-1, -FRC_MUL);
				fetch(1) <= '0';
			-- and the linear part is last
			elsif fetch(0) = '1' then
				K <= resize(Kp_i, INT_MUL-1, -FRC_MUL);
				if force_i = '1' then -- if force_i = 1, no need to accumulate the all previous data to get the first one when the GL module will be able to work, so the current is used
					data <= resize(data_i, INT_MUL-1, -FRC_MUL);
				else
					data <= resize(data_delay_line(data_delay_line'left), INT_MUL-1, -FRC_MUL);
				end if;
				fetch(0) <= '0';
			end if;
			-- when the common multiplier result is ready - accuulate it, so it will be (Kd*D + Ki*I + Kp*X)
			if K_common_mul_sync_result_sync = '1' then
				result_acc <= resize(result_acc, 2*INT-2, -2*FRC) + resize(to_sfixed(K_common_mul_sync_result, 2*INT_MUL-1, -2*FRC_MUL), 2*INT-2, -2*FRC);
			end if;
			if start_i = '1' then -- initial reset the result accumulator to 0
				result_acc <= to_sfixed(0, 2*INT-1, -2*FRC);
			end if;
			K_common_mul_sync <= fetch(2) or fetch(1) or fetch(0); -- the sync signal for The common multiplier is when at least one result is still not processed yet
			-- Actually, the GL results are available simultaneously so the results from the output Common multiplier will at sequence of 3 clock cycles
			-- So it is valid just to delay the output sync signal and when the all bit on this delay line will be high - the final result is complete
			K_common_mul_sync_result_sync_vector <= K_common_mul_sync_result_sync_vector(1 downto 0) & K_common_mul_sync_result_sync;
			valid_o <= '0';
			if K_common_mul_sync_result_sync_vector = "111" then
				data_o <= resize(result_acc, INT-1, -FRC);
				valid_o <= '1';
			end if;
		end if;
	end process;

	-- The Common pipelined Multiplier - it is valid to pass the new data at each clock cycle because of an internal pipeline
--virtex5_gen: if family = "virtex5" generate
--	K_common_mul : mult59x59_pipe
--	port map (
--		  clk_i => clk_i
--		, clr_i => clr_i
--		, data0_i => to_slv(resize(K, INT_MUL-1, -FRC_MUL))
--		, data1_i => to_slv(resize(data, INT_MUL-1, -FRC_MUL))
--		, sync_i => K_common_mul_sync -- need to organize a sequence
--		, data_o => K_common_mul_sync_result
--		, sync_o => K_common_mul_sync_result_sync
--	);

--end generate;
--spartan6_gen: if family = "spartan6" generate
	K_common_mul : mult35x35_pipe
	generic map (
		 family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => to_slv(resize(K, INT_MUL-1, -FRC_MUL))
		, data1_i => to_slv(resize(data, INT_MUL-1, -FRC_MUL))
		, sync_i => K_common_mul_sync -- need to organize a sequence
		, data_o => K_common_mul_sync_result
		, sync_o => K_common_mul_sync_result_sync
	);

--end generate;
--	K_real <= to_real(K);
--	data_real <= to_real(data);

end;