-- based on linearisation architecture scheme form 2.pdf p.4 figure 5.b

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

entity linear_aproximation is
generic (
	  N : integer := 64
	; INT : integer := 8
	; FRC : integer := 24
	; family : string := "spartan6"
);
port (
	  clk_i : in std_logic                   -- input clock
	; clr_i : in std_logic                   -- synchronous reset
	; C0 : in sfixed(INT-1 downto -FRC)      -- the C0 linearization constant
	; C1 : in sfixed(INT-1 downto -FRC)      -- the C1 linearization constant
	; H : in sfixed(INT-1 downto -FRC)       -- the Step
	; data_i : in sfixed(INT-1 downto -FRC)  -- the input data samples stream
	; sync_i : in std_logic                  -- the sync validation signal of the input data
	; data_o : out sfixed(INT-1 downto -FRC) -- the linearization results for the each data sample
	; sync_o : out std_logic                 -- the sync output validation signal
);
end entity;

architecture rtl of linear_aproximation is

	component mult32x32_pipe is -- DSP-slice based multiplier 32x32
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(31 downto 0)
		; data1_i: in std_logic_vector(31 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(63 downto 0)
		; sync_o: out std_logic
	);
	end component mult32x32_pipe;

	component mult35x35_pipe is -- DSP-slice based multiplier 35x35
	generic (
		  family : string := "spartan6"
	);
	port (
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(34 downto 0)
		; data1_i: in std_logic_vector(34 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(69 downto 0)
		; sync_o: out std_logic
	);
	end component mult35x35_pipe;

	signal sync_C1H, sync_C1H_o, sync_C0, sync_C0_o : std_logic;
	signal data_i_slv, data_shifted_slv : std_logic_vector(INT + FRC - 1 downto 0);
	signal XC1H_slv, XC0_slv, C1H_slv : std_logic_vector(2*(INT + FRC) - 1 downto 0);
	signal sync_C1H_o_ff : std_logic_vector(1 downto 0) := "00";
	signal XC1H, XC0_ff, XC0_ff2, XC0, C1H, data_shifted : sfixed(INT-1 downto -FRC);
	signal X_ACC, X_ACC_NX, XC1H_ACC : sfixed(INT downto -FRC);
	-- log2(N)
	constant N_LOG2 : integer := integer(log2(real(N)));
	signal sync_ff : std_logic := '0';
	signal data_sum : sfixed(INT downto -FRC);

	-- debug only signals
--	signal data_sum_real : real;
--	signal X_ACC_real, X_ACC_NX_real, XC1H_ACC_real, XC1H_real, XC0_real, C1H_real, data_shifted_real : real;
	signal C0_MUL_d0, C0_MUL_d1, XC1H_MUL_d0, XC1H_MUL_d1, C1H_MUL_d0, C1H_MUL_d1 : std_logic_vector(34 downto 0);
	signal C0_MUL_p, XC1H_MUL_p, C1H_MUL_p : std_logic_vector(69 downto 0);

	signal X_ACC_slv, X_ACC_NX_slv : std_logic_vector(INT+FRC downto 0);
	signal C0_slv, C1H_slv32, C1_slv, H_slv : std_logic_vector(INT+FRC-1 downto 0);
	
	function replicate (input : in std_logic; size : in positive) return std_logic_vector is
		variable r : std_logic_vector(size-1 downto 0);
	begin
		for i in r'range loop
			r(i) := input;
		end loop;
		return r;
	end function;
begin

	-- Multiplication by N. It is supposed that N is a power of 2. So this multiplication is equalto a shift
	data_shifted_slv(N_LOG2-1 downto 0) <= (others => '0');
	data_shifted_slv(INT+FRC-1) <= data_i_slv(INT+FRC-1);
	data_shifted_slv(INT+FRC-2 downto N_LOG2) <= data_i_slv(INT+FRC-N_LOG2-2 downto 0);

	-- type conversions and resizing
	data_i_slv <= to_slv(data_i); -- from std_logic_vector to sfixed
	XC1H <= resize(to_sfixed(XC1H_slv, 2*INT-1, -2*FRC), INT-1, -FRC); -- to sfixed and resize to the general fixed point size: 32::[7 : -24]
	XC0 <= resize(to_sfixed(XC0_slv, 2*INT-1, -2*FRC), INT-1, -FRC);   -- the multiplication results have a double size (64) and it must be solved to 32
	C1H <= resize(to_sfixed(C1H_slv, 2*INT-1, -2*FRC), INT-1, -FRC);

	-- output results
	sync_o <= sync_C1H_o_ff(1); --sync output signal - a validation signal
	data_o <= resize(data_sum, INT-1, -FRC); -- the linearisation result for the current stage, i.e. which is computed based on the corresponding input - data(i) and the accumulators stages

	-- Debug only signals	
--	data_sum_real <= to_real(data_sum);
--	X_ACC_real <= to_real(X_ACC);
--	X_ACC_NX_real <= to_real(X_ACC_NX);
--	XC1H_ACC_real <= to_real(XC1H_ACC);
--	XC1H_real <= to_real(XC1H);
--	XC0_real <= to_real(XC0);
--	C1H_real <= to_real(C1H);
--	data_shifted_real <= to_real(data_shifted);
	---------------------------------------------

	data_shifted <= to_sfixed(data_shifted_slv, INT-1, -FRC);

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			-- C0*<input acc> delay to synchronize it with the other computations
			XC0_ff <= XC0;
			XC0_ff2 <= XC0_ff;
			-- resulting adder
			data_sum <= XC0_ff2 + resize(XC1H_ACC, INT-1, -FRC);
			-- synchronize the input multiplied by the N with the input accumulator
			-- the C1H multiplication Accumulator
			if sync_C1H_o = '1' then
				XC1H_ACC <= resize(XC1H_ACC, INT-1, -FRC) + XC1H;
			end if;
			-- the input accumulator
			if sync_i = '1' then
				X_ACC <= resize(X_ACC, INT-1, -FRC) + data_i;
			end if;
			-- the nput acc + the input multiplied to N
			if sync_i = '1' then
				X_ACC_NX <= resize(X_ACC, INT-1, -FRC) + data_shifted;
			end if;
			-- sync signals to organize a pipeline
			sync_ff <= sync_i;
			sync_C1H <= sync_i;--sync_ff;
			sync_C1H_o_ff <= sync_C1H_o_ff(0) & sync_C1H_o;
			-- synchronous reset
			if clr_i = '1' then
				XC1H_ACC <= to_sfixed(0, INT, -FRC);
				X_ACC <= to_sfixed(0, INT, -FRC);
				X_ACC_NX <= to_sfixed(0, INT, -FRC);
			end if;
		end if;
	end process;

	X_ACC_slv <= to_slv(X_ACC);
	X_ACC_NX_slv <= to_slv(X_ACC_NX);
	C1H_slv32 <= to_slv(C1H);
	C0_slv <= to_slv(C0);
	C1_slv <= to_slv(C1);
	H_slv <= to_slv(H);

	-- pipelined DSP-slice based multipliers
	C0_MUL_d0 <= replicate(X_ACC_slv(INT+FRC),2) & X_ACC_slv;
	C0_MUL_d1 <= replicate(C0_slv(INT+FRC-1),3) & C0_slv;
	-- input ACC * C0
	C0_MUL : mult35x35_pipe
	generic map (
		 family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => C0_MUL_d0
		, data1_i => C0_MUL_d1
		, sync_i => sync_C0
		, data_o => C0_MUL_p--XC0_slv
		, sync_o => sync_C0_o
	);
	XC0_slv <= C0_MUL_p(63 downto 0);

	XC1H_MUL_d0 <= replicate(X_ACC_NX_slv(INT+FRC),2) & X_ACC_NX_slv;
	XC1H_MUL_d1 <= replicate(C1H_slv32(INT+FRC-1),3) & C1H_slv32;
	-- C1H multiplication ACC
	XC1H_MUL : mult35x35_pipe
	generic map (
		 family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => XC1H_MUL_d0
		, data1_i => XC1H_MUL_d1
		, sync_i => sync_C1H
		, data_o => XC1H_MUL_p--XC1H_slv
		, sync_o => sync_C1H_o
	);
	XC1H_slv <= XC1H_MUL_p(63 downto 0);
	
	C1H_MUL_d0 <= replicate(C1_slv(INT+FRC-1),3) & C1_slv;
	C1H_MUL_d1 <= replicate(H_slv(INT+FRC-1),3) & H_slv;
	-- C1*H - this may be replaced to the constant if need.
	C1H_MUL : mult35x35_pipe
	generic map (
		 family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => C1H_MUL_d0
		, data1_i => C1H_MUL_d1
		, sync_i => '1'
		, data_o => C1H_MUL_p--C1H_slv
		, sync_o => open
	);
	C1H_slv <= C1H_MUL_p(63 downto 0);
	
	-----------------------------------------------

end;