library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

-- The GL Core
-- Perform the GL algorithm for the N-sequnced data samples with N weight and linear approximation module
-- the main idea:
-- at first need to load the weights
-- then to pass the data sample one by one sync'ed by the validation strobe. These sample will go to the data delay buffer which is to orginize a FIR-like delay line
-- When the start input is received it starts the processing
-- The data samples from the data delay buffer and the weights from the weights container is passed synchronously to the computation core
-- After the all multiplications are computed and accumulated for the current data delay bufer state

entity GL_core is
generic (
	  N : integer := 64
	; INT : integer := 8
	; FRC : integer := 24
	; family : string := "spartan6"
);
port (
	  clk_i : in std_logic                       -- clock
	; clr_i : in std_logic                       -- synchronous reset
	; C0 : in sfixed(INT-1 downto -FRC)          -- C0 linear approx constant
	; C1 : in sfixed(INT-1 downto -FRC)          -- C1 linear approx constant
	; H : in sfixed(INT-1 downto -FRC)           -- H step constant
	; data_pscl_i : in std_logic                 -- input sample prescaler signal or validation pulse that is the same
	; start_i : in std_logic                     -- start pulse - is to start the GL algorithm with the current state of data registers and delay line and the weights container
	; ready_to_receive_data_o : out std_logic    -- is available to receive a next data, i.e. not in the processing state
	; data_control_full_o : out std_logic        -- the data delay buffer is full (just in case)
	; data_i : in sfixed(INT-1 downto -FRC)      -- the input data samples which are sync'ed by the data_pscl_i
	; weight_pscl_i : in std_logic               -- sync pulses for the weights loading
	; weight_i : in sfixed(INT-1 downto -FRC)    -- the weights
	; weights_containers_full_o : out std_logic  -- the integral weights container is full
	; force_i : in std_logic                     -- 1 - ignore the weights container's state and the partially filled data delay buffer
	; valid_o : out std_logic                    -- data valid strobe
	; data_o : out sfixed(2*INT-1 downto -2*FRC) -- the GL result
);
end entity GL_core;

architecture rtl of GL_core is

	component data_feeding_control is
	generic (
		  N : integer := 64     -- A number of the weights - i.e. delay stages
		; INT : integer := 8
		; FRC : integer := 24
		; CYCLIC_BUFFER : boolean := false
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; restart_i : in std_logic
		; data_pscl_i : in std_logic
		; ready_o : out std_logic -- is available to receive a next data
		; empty_o : out std_logic
		; start_i : in std_logic  -- start to work
		; data_i : in sfixed(INT-1 downto -FRC)
		; complete_o : out std_logic -- data feeding to an extarnal module is complete
		; ready_i : in std_logic  -- an external module ready to get a new data
		; valid_o : out std_logic -- data valid strobe
		; data_o : out sfixed(INT-1 downto -FRC)
	);	
	end component data_feeding_control;

	component GL_math_core is
	generic (
		  N : integer := 64     -- A number of the weights - i.e. delay stages
		; INT : integer := 8
		; FRC : integer := 24
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; C0 : in sfixed(INT-1 downto -FRC)
		; C1 : in sfixed(INT-1 downto -FRC)
		; H : in sfixed(INT-1 downto -FRC)
		; valid_i : in std_logic -- data valid strobe
		; ready_o : out std_logic  -- a data ready strobe
		; data_i : in sfixed(INT-1 downto -FRC)
		; weight_i : in sfixed(INT-1 downto -FRC)
		; data_o : out sfixed(2*INT-1 downto -2*FRC)
	);
	end component GL_math_core;

	signal computation_ready_ff, computation_ready : std_logic := '0';
	signal computation_complete : std_logic;
	signal weights_containers_full : std_logic := '0';
	signal population_processing : std_logic := '0';
	signal data_pscl : std_logic;
	signal data_control_ready, ready_to_receive_data, data_control_empty : std_logic;
	signal data_pscl_ff : std_logic_vector(1 downto 0) := "00";
	signal start : std_logic;
	signal data_smaple : sfixed(INT-1 downto -FRC);
	signal data_smaple_valid : std_logic;

	signal
		weight_control_ready_falling, weight_control_ready,
		weight_control_ready_ff, ready_to_weight, weight_enable,
		weight_control_complete, weight_control_empty, weight_control_empty_ff,
		weight_control_empty_falling : std_logic;

	type sfixed_array is array (integer range <>) of sfixed(INT-1 downto -FRC);
	signal weights_vector : sfixed_array(0 to N-1);

	signal weight : sfixed(INT-1 downto -FRC);

	signal weight_control_ready_vector_any : std_logic;
	signal all_stages_is_passed, bound_is_reached : std_logic;
	signal start_enabled : std_logic;

	signal data_result : sfixed(2*INT-1 downto -2*FRC);
--	signal data_result_real, data_smaple_real, weight_real : real;

begin

	--weights_containers_full <= not weight_control_ready_vector_any;
	weights_containers_full_o <= not weight_control_empty; --weights_containers_full;
	data_control_full_o <= not data_control_empty;

	computation_complete <= computation_ready_ff and not computation_ready;
	ready_to_receive_data <= data_control_ready;

	data_pscl <= data_pscl_i and ((not weight_control_empty and not data_control_empty) or force_i) and ready_to_receive_data;

	ready_to_receive_data_o <= ready_to_receive_data;

	--start <= data_pscl_ff(0) and not data_pscl_ff(1);

	process (clk_i)
	begin
		if rising_edge(clk_i) then

			weight_control_ready_ff <= weight_control_ready;
			weight_control_ready_falling <= weight_control_ready_ff and not weight_control_ready;

			weight_control_empty_ff <= weight_control_empty;
			weight_control_empty_falling <= weight_control_empty_ff and not weight_control_empty;

			computation_ready_ff <= computation_ready;
			--data_pscl_ff <= data_pscl_ff(0) & data_pscl;

			valid_o <= computation_complete;
					
		end if;
	end process;

	-- GL computation core
	GL_math_core_inst : GL_math_core
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => start_i or clr_i -- must be resetted at the each start because it will be a new computation with the new data delay buffer state
		, C0 => C0
		, C1 => C1
		, H => H
		, valid_i => data_smaple_valid
		, ready_o => computation_ready
		, data_i => data_smaple
		, weight_i => weight
		, data_o => data_result
	);

--	data_smaple_real <= to_real(data_smaple);
--	weight_real <= to_real(weight);
--	data_result_real <= to_real(data_result);
	data_o <= data_result;

	-- Data delay buffer
	data_control : data_feeding_control
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, CYCLIC_BUFFER => true
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => '0'
		, data_pscl_i => data_pscl
		, ready_o => data_control_ready
		, empty_o => data_control_empty
		, start_i => start_i
		, data_i => data_i
		, complete_o => all_stages_is_passed
		, ready_i => '1'
		, valid_o => data_smaple_valid
		, data_o => data_smaple
	);

	-- Weight container
	weight_control : data_feeding_control
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, restart_i => '0'
		, data_pscl_i => weight_pscl_i
		, empty_o => weight_control_empty
		, ready_o => weight_control_ready
		, start_i => start_i
		, data_i => weight_i
		, complete_o => weight_control_complete
		, ready_i => '1'
		, valid_o => open
		, data_o => weight
	);

end;