library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;
library unisim;
use unisim.vcomponents.all;

-- Data delay buffer and container module
-- The module stores the data that is validated by the data_pscl_i signal
-- When the start signal is received the module outputs the all content of it's intetnal buffer if the ready_i is high.
-- When the all content is transmitted the module return to the IDLE state and ready to get the next input data

entity data_feeding_control is
generic (
	  N : integer := 64     -- A number of the weights - i.e. delay stages
	; INT : integer := 8
	; FRC : integer := 24
	; CYCLIC_BUFFER : boolean := false -- switch to true for the cyclic buffer as the data delay buffer. For the Weight containers must be False
	; family : string := "spartan6"
);
port (
	  clk_i : in std_logic                    -- clock
	; clr_i : in std_logic                    -- synchronous reset
	; restart_i : in std_logic
	; data_pscl_i : in std_logic              -- the input data validation signal
	; ready_o : out std_logic                 -- is available to receive a next data
	; empty_o : out std_logic                 -- the internal buffer is not full
	; start_i : in std_logic                  -- start to work
	; data_i : in sfixed(INT-1 downto -FRC)   -- the input data to store
	; complete_o : out std_logic              -- data feeding to an extarnal module is complete
	; ready_i : in std_logic                  -- an external module ready to get a new data
	; valid_o : out std_logic                 -- data valid strobe
	; data_o : out sfixed(INT-1 downto -FRC)  -- the output data
);	
end entity;

architecture rtl of data_feeding_control is
	-- the internal container/buffer
	type data_container_t is array (integer range <>) of sfixed(INT-1 downto -FRC);
	signal data_container : data_container_t(0 to N-1);
	-----------------------------------------------------------------------------------
	constant SIZE : integer := integer(log2(real(N)));
	signal data_input_cnt : unsigned(SIZE-1 downto 0) := (others => '0');
	signal data_output_cnt : unsigned(SIZE-1 downto 0) := (others => '0');
	signal empty, ready : std_logic;
	signal job : std_logic := '0';
	type state_t is (IDLE, SEND_DATA, WAIT_READY);
	signal state : state_t := IDLE;
	signal deposit : unsigned(SIZE-1 downto 0) := (others => '0');
	signal cycle_shift : unsigned(SIZE-1 downto 0) := (others => '0');
	signal raddr : unsigned(SIZE-1 downto 0) := (others => '0');
	signal first_run : boolean := true;
	signal ADDRA : std_logic_vector(13 downto 0) := (others => '0');
	signal ADDRB : std_logic_vector(13 downto 0) := (others => '0');
	signal DOB : std_logic_vector(31 downto 0);
	signal ENB : std_logic;
begin

virtex5_gen: if family = "virtex5" generate
	CONTAINER : RAMB18SDP
	generic map (
		DO_REG => 0, -- Optional output register (0 or 1)
		INIT => X"000000000", -- Initial values on output port
		SIM_COLLISION_CHECK => "ALL", -- Collision check enable "ALL", "WARNING_ONLY",
		-- "GENERATE_X_ONLY" or "NONE"
		SIM_MODE => "SAFE", -- Simulation: "SAFE" vs "FAST", see "Synthesis and Simulation
		-- Design Guide" for details
		SRVAL => X"000000000", -- Set/Reset value for port output
		-- The following INIT_xx declarations specify the initial contents of the RAM
		INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
		-- The next set of INITP_xx are for the parity bits
		INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000"
	)
	port map (
		DO => DOB, -- 32-bit Data Output
		DOP => open, -- 4-bit Parity Output
		RDCLK => clk_i, -- 1-bit read port clock
		RDEN => ENB, -- 1-bit read port enable
		REGCE => '1', -- 1-bit register enable input
		SSR => '0', -- 1-bit synchronous output set/reset input
		WRCLK => clk_i, -- 1-bit write port clock
		WREN => ready, -- 1-bit write port enable
		WRADDR => ADDRA(8 downto 0), -- 9-bit write port address input
		RDADDR => ADDRB(8 downto 0), -- 9-bit read port address input
		DI => to_slv(data_i), -- 32-bit data input
		DIP => x"0", -- 4-bit parity data input
		WE => data_pscl_i & data_pscl_i & data_pscl_i & data_pscl_i -- 4-bit write enable input
	);

	process (data_input_cnt, cycle_shift, data_output_cnt, raddr)
	begin
		ADDRA <= (others => '0');
		ADDRA(SIZE-1 downto 0) <= std_logic_vector(data_input_cnt);
		ADDRB <= (others => '0');
		if CYCLIC_BUFFER then
			ADDRB(SIZE-1 downto 0) <= std_logic_vector(to_unsigned((to_integer(raddr)+to_integer(cycle_shift)) mod N, SIZE));
		else
			ADDRB(SIZE-1 downto 0) <= std_logic_vector(to_unsigned((to_integer(data_output_cnt)) mod N, SIZE));
		end if;
	end process;

end generate;

spartan6_gen: if family = "spartan6" generate
	process (data_input_cnt, cycle_shift, data_output_cnt, raddr)
	begin
		ADDRA <= (others => '0');
		ADDRA(SIZE-1+5 downto 0+5) <= std_logic_vector(data_input_cnt);
		ADDRB <= (others => '0');
		if CYCLIC_BUFFER then
			ADDRB(SIZE-1+5 downto 0+5) <= std_logic_vector(to_unsigned((to_integer(raddr)+to_integer(cycle_shift)) mod N, SIZE));
		else
			ADDRB(SIZE-1+5 downto 0+5) <= std_logic_vector(to_unsigned((to_integer(data_output_cnt)) mod N, SIZE));
		end if;
	end process;

	CONTAINER : RAMB16BWER
	generic map (
		-- DATA_WIDTH_A/DATA_WIDTH_B: 0, 1, 2, 4, 9, 18, or 36
		DATA_WIDTH_A => 36,
		DATA_WIDTH_B => 36,
		-- DOA_REG/DOB_REG: Optional output register (0 or 1)
		DOA_REG => 0,
		DOB_REG => 0,
		-- EN_RSTRAM_A/EN_RSTRAM_B: Enable/disable RST
		EN_RSTRAM_A => FALSE,
		EN_RSTRAM_B => FALSE,
		-- INITP_00 to INITP_07: Initial memory contents.
		INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
		-- INIT_00 to INIT_3F: Initial memory contents.
		INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
		INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
		-- INIT_A/INIT_B: Initial values on output port
		INIT_A => X"000000000",
		INIT_B => X"000000000",
		-- INIT_FILE: Optional file used to specify initial RAM contents
		INIT_FILE => "NONE",
		-- RSTTYPE: "SYNC" or "ASYNC"
		RSTTYPE => "SYNC",
		-- RST_PRIORITY_A/RST_PRIORITY_B: "CE" or "SR"
		RST_PRIORITY_A => "CE",
		RST_PRIORITY_B => "CE",
		-- SIM_COLLISION_CHECK: Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
		SIM_COLLISION_CHECK => "ALL",
		-- SIM_DEVICE: Must be set to "SPARTAN6" for proper simulation behavior
		SIM_DEVICE => "SPARTAN6",
		-- SRVAL_A/SRVAL_B: Set/Reset value for RAM output
		SRVAL_A => X"000000000",
		SRVAL_B => X"000000000",
		-- WRITE_MODE_A/WRITE_MODE_B: "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE"
		WRITE_MODE_A => "WRITE_FIRST",
		WRITE_MODE_B => "WRITE_FIRST"
	)
	port map (
		-- Port A Data: 32-bit (each) output: Port A data
		DOA => open, -- 32-bit output: A port data output
		DOPA => open, -- 4-bit output: A port parity output
		-- Port B Data: 32-bit (each) output: Port B data
		DOB => DOB, -- 32-bit output: B port data output
		DOPB => open, -- 4-bit output: B port parity output
		-- Port A Address/Control Signals: 14-bit (each) input: Port A address and control signals
		ADDRA => ADDRA, -- 14-bit input: A port address input
		CLKA => clk_i, -- 1-bit input: A port clock input
		ENA => ready, -- 1-bit input: A port enable input
		REGCEA => '1', -- 1-bit input: A port register clock enable input
		RSTA => '0', -- 1-bit input: A port register set/reset input
		WEA => data_pscl_i & data_pscl_i & data_pscl_i & data_pscl_i, -- 4-bit input: Port A byte-wide write enable input
		-- Port A Data: 32-bit (each) input: Port A data
		DIA => to_slv(data_i), -- 32-bit input: A port data input
		DIPA => (others => '0'), -- 4-bit input: A port parity input
		-- Port B Address/Control Signals: 14-bit (each) input: Port B address and control signals
		ADDRB => ADDRB, -- 14-bit input: B port address input
		CLKB => clk_i, -- 1-bit input: B port clock input
		ENB => ENB, -- 1-bit input: B port enable input
		REGCEB => '1', -- 1-bit input: B port register clock enable input
		RSTB => '0', -- 1-bit input: B port register set/reset input
		WEB => (others => '0'), -- 4-bit input: Port B byte-wide write enable input
		-- Port B Data: 32-bit (each) input: Port B data
		DIB => (others => '0'), -- 32-bit input: B port data input
		DIPB => (others => '0') -- 4-bit input: B port parity input
	);
end generate;
	ENB <= '1' when state = SEND_DATA else '0';

	data_o <= to_sfixed(DOB, INT-1, -FRC);

	empty_o <= empty;
	--ready_o <= ready;

	empty <= '1' when deposit < to_unsigned(N, SIZE) else '0'; -- is not full
	ready <= '1' when state = IDLE else '0'; -- ready to get the next data, the same as the module is in the IDLE state

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			ready_o <= ready; --not empty;
			valid_o <= '0';
			complete_o <= '0';
			-- If in the CYCLIC_BUFFER mode the kind of LIFO is implemented: the data is stored in the cells: 0,1,2,3,4,5,
			-- but the read from the cells at each start (next output cycle) must be starts from the: 0,1,2,3, ... and count down
			-- i.e. at the first cycle the read will be from the cells: 0, 63, 62, 61, .. 1
			-- at the second cycle: 1, 0, 63, 62, .. 2
			-- and so on.
			--if CYCLIC_BUFFER then
			--	data_o <= data_container((to_integer(raddr)+to_integer(cycle_shift)) mod N);
			--else
			--	data_o <= data_container((to_integer(data_output_cnt)) mod N);
			--end if;
			if start_i = '1' then -- if the start signal is received - go to check the ready signal from an external receiver module
				data_output_cnt <= (others => '0');
				raddr <= (others => '0');
				job <= '1';
				state <= WAIT_READY;
			end if;
			if job = '1' then
				case state is
					when WAIT_READY =>
						if ready_i = '1'  then -- if ready again - go to data sending
							state <= SEND_DATA;
						end if;
					when SEND_DATA =>
						--if CYCLIC_BUFFER then
						--	data_o <= data_container((to_integer(raddr)+to_integer(cycle_shift)) mod N);
						--else
						--	data_o <= data_container((to_integer(data_output_cnt)) mod N);
						--end if;
						valid_o <= '1';
						if data_output_cnt = to_unsigned(N-1, SIZE) then -- when the all data is outputed
							state <= IDLE;
							first_run <= false;
							--cycle_shift <= cycle_shift + 1; -- starting shift of read in the CYCLIC_BUFFER mode
							--if cycle_shift = to_unsigned(N-1, SIZE) then
							--	cycle_shift <= (others => '0');
							--end if;
						else
							if ready_i = '0' then -- if the ready signal goes low - go to waitng the ready input
								state <= WAIT_READY;
							end if;
							-- looks a bit wired but this is the result of multifunctionality. This signals are not big so I think there will not any hard timing problems
							-- I will redesign this if need.
							data_output_cnt <= data_output_cnt + 1;
							raddr <= to_unsigned(N-1, SIZE) - data_output_cnt;
						end if;
					when IDLE => -- complete when IDLE
						job <= '0';
						complete_o <= '1';
						--data_input_cnt <= (others=> '0');
					when others =>
						state <= IDLE;
				end case;
			end if;
			if ready = '1' then -- when ready to get the next data sample - store it into the internal buffer
				if data_pscl_i = '1' then
					--data_container(to_integer(data_input_cnt)) <= data_i;
					data_input_cnt <= data_input_cnt + 1;
					if empty = '1' then
						deposit <= deposit + 1;
					end if;
					if not first_run then
						cycle_shift <= cycle_shift + 1; -- starting shift of read in the CYCLIC_BUFFER mode
						if cycle_shift = to_unsigned(N-1, SIZE) then
							cycle_shift <= (others => '0');
						end if;
					end if;
				end if;
			end if;
			if restart_i = '1' then
				state <= IDLE;
				deposit <=(others => '0');
				job <= '0';
				complete_o <= '0';
				data_input_cnt <= (others => '0');
				data_output_cnt <= (others => '0');
			end if;
			if clr_i = '1' then
				first_run <= true;
				--for i in 0 to N-1 loop
				--	data_container(i) <= to_sfixed(0.0, INT-1, -FRC);
				--end loop;
				cycle_shift <= (others => '0');
				state <= IDLE;
				deposit <=(others => '0');
				job <= '0';
				complete_o <= '0';
				data_input_cnt <= (others=> '0');
			end if;
		end if;
	end process;

end;