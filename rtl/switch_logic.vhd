-- This module is not used

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
--library ieee_proposed;
--use ieee_proposed.fixed_pkg.all;
use ieee.fixed_pkg.all;

entity switch_logic is 
generic (
	  N : integer := 50
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic
	; bound_i : in std_logic_vector(log2(N)-1 downto 0)
	; controlA : in std_logic
	; controlB : in std_logic
	; controlC : in std_logic
	; controlD : in std_logic
	; controlA_vector : out std_logic_vector(N-1 downto 0)
	; controlB_vector : out std_logic_vector(N-1 downto 0)
	; controlC_vector : out std_logic_vector(N-1 downto 0)
	; controlD_vector : out std_logic_vector(N-1 downto 0)
	; switch_event_vector : in std_logic_vector(N-1 downto 0)
	; enable_o : out std_logic_vector(N-1 downto 0)
	; bound_is_reached : out std_logic := '0'
);
end entity;

architecture rtl of switch_logic is

	signal enable : std_logic_vector(N-1 downto 0) := (0 => '1', others => '0');
	signal switch_event_vector_masked : std_logic_vector(N-1 downto 0);
	signal switch_event : std_logic;

	component or_recursive is
	generic (
		  order : integer := 1 -- the current order of the "tree" - actually the width of the input data
	);
	port (
		  a : in std_logic_vector(order-1 downto 0)
		; z : out std_logic
	);
	end component or_recursive;

	signal bound_cnt : unsigned(log2(N)-1 downto 0) := (others => '0');
begin
	
	output_gen: for i in N-1 downto 0 generate
		controlA_vector(i) <= controlA and enable(i);
		controlB_vector(i) <= controlB and enable(i);
		controlC_vector(i) <= controlC and enable(i);
		controlD_vector(i) <= controlD and enable(i);
		switch_event_vector_masked(i) <= switch_event_vector(i) and enable(i);
	end generate output_gen;

	complete_result : or_recursive
	generic map (
		  order => N
	)
	port map (
		  a => switch_event_vector_masked
		, z => switch_event
	);

	enable_o <= enable;
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then

			if switch_event = '1' then
				if bound_cnt /= unsigned(bound_i) then
					enable <= enable(enable'left-2 downto 1) & enable(enable'left);
					bound_cnt <= bound_cnt + 1;
				end if;
			end if;

			if bound_cnt = unsigned(bound_i) then
				bound_is_reached <= '1';
			else
				bound_is_reached <= '0';
			end if;

			if clr_i = '1' then
				bound_cnt <= (others => '0');
				enable <= (others => '0');
				enable(0) <= '1';
			end if;
		end if;
	end process;

end;