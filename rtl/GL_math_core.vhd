library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

-- The computation core of the GL module
-- it multiply the input data sample to the weight, perform the linear approximation
-- and computes the result for the current sample and weight

entity GL_math_core is
generic (
	  N : integer := 64     -- A number of the weights - i.e. delay stages
	; INT : integer := 8
	; FRC : integer := 24
	; family : string := "spartan6"
);
port (
	  clk_i : in std_logic                       -- clock
	; clr_i : in std_logic                       -- synchronous reset - need to use to reset the accumulator before the next processng cycle. The single cycle - is the all N pass with weights for the current data delay buffer state
	; C0 : in sfixed(INT-1 downto -FRC)          -- C0 linear approx constant
	; C1 : in sfixed(INT-1 downto -FRC)          -- C1 linear approx constant
	; H : in sfixed(INT-1 downto -FRC)           -- H step constant
	; valid_i : in std_logic                     -- data valid strobe
	; ready_o : out std_logic                    -- a data ready strobe
	; data_i : in sfixed(INT-1 downto -FRC)      -- the input data sample
	; weight_i : in sfixed(INT-1 downto -FRC)    -- the weight
	; data_o : out sfixed(2*INT-1 downto -2*FRC) -- the result
);
end entity GL_math_core;

architecture rtl of GL_math_core is

	component linear_aproximation is
	generic (
		  N : integer := 64
		; INT : integer := 8
		; FRC : integer := 24
		; family : string := "spartan6"
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; C0 : in sfixed(INT-1 downto -FRC)
		; C1 : in sfixed(INT-1 downto -FRC)
		; H : in sfixed(INT-1 downto -FRC)
		; data_i : in sfixed(INT-1 downto -FRC)
		; sync_i : in std_logic
		; data_o : out sfixed(INT-1 downto -FRC)
		; sync_o : out std_logic
	);
	end component;

	component mult32x32_pipe is -- DSP-slice based multiplier 32x32
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(31 downto 0)
		; data1_i: in std_logic_vector(31 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(63 downto 0)
		; sync_o: out std_logic
	);
	end component mult32x32_pipe;

	component mult35x35_pipe is -- DSP-slice based multiplier 35x35
	generic (
		  family : string := "spartan6"
	);
	port (
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(34 downto 0)
		; data1_i: in std_logic_vector(34 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(69 downto 0)
		; sync_o: out std_logic
	);
	end component mult35x35_pipe;

	type fetch_state_t is (IDLE, FETCH);
	signal state : fetch_state_t := IDLE;

	signal data, weight, result_mul_ff3, result_mul_ff4, result_mul_ff2, result_mul_ff1, result_mul, result_linear_aproximation : sfixed(INT-1 downto -FRC);
	signal synco_linear_aproximation : std_logic;
	signal synco_linear_aproximation_ff : std_logic := '0';
	signal result_data, acc : sfixed(2*INT-1 downto -2*FRC);
	signal result_mul_slv : std_logic_vector(2*(INT + FRC) - 1 downto 0);
	signal sync : std_logic := '0';
--	signal result_mul_ff3_real, result_mul_ff4_real, result_mul_ff2_real, result_mul_ff1_real, result_mul_real, result_data_real, acc_real, weight_real : real;
	signal data35, weight35 : std_logic_vector(34 downto 0);
	signal p70 : std_logic_vector(69 downto 0);
begin

	data_o <= acc;
	result_mul <= resize(to_sfixed(result_mul_slv, 2*INT-1, -2*FRC), INT-1, -FRC);
	-- debugging signal of the type Real
--	result_mul_ff3_real <= to_real(result_mul_ff4);
--	result_mul_ff4_real <= to_real(result_mul_ff3);
--	result_mul_ff2_real <= to_real(result_mul_ff2);
--	result_mul_ff1_real <= to_real(result_mul_ff1);
--	result_mul_real <= to_real(result_mul);
--	result_data_real <= to_real(result_data);
--	acc_real <= to_real(acc);
--	weight_real <= to_real(weight);
	--------------------------------------------------------------------
	
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			-- delay to the final adder latency
			synco_linear_aproximation_ff <= synco_linear_aproximation;
			ready_o <= synco_linear_aproximation_ff;
			-- delay the multiplier result to synchronize it with the linear approximation module's result
			result_mul_ff4 <= result_mul_ff3;
			result_mul_ff3 <= result_mul_ff2;
			result_mul_ff2 <= result_mul_ff1;
			result_mul_ff1 <= result_mul;
			-- FSM switch control
			case state is
				when IDLE =>
					if valid_i = '1' then -- when the input is valid - go to the FETCH state where the input data and weight are fetched and the computations are executed
						state <= FETCH;
					end if;
				when FETCH =>
					if valid_i = '0'then -- when the input is no more valid - stop the processing and go to the IDLE state
						state <= IDLE;
					end if;
				when others =>
					state <= IDLE;
			end case;

			data <= data_i;
			weight <= weight_i;

			-- add the weight mul result with the linera approx result
			result_data <= resize(result_linear_aproximation + result_mul_ff3, 2*INT-1, -2*FRC);
			-- accumulate by the currently computed result
			if synco_linear_aproximation_ff = '1' then
				acc <= resize(acc, 2*INT-2, -2*FRC) + resize(result_data, 2*INT-2, -2*FRC);
			end if;
			if clr_i = '1' then -- reset the accumulator
				acc <= to_sfixed(0, 2*INT-1, -2*FRC);
				state <= IDLE;
			end if;
		end if;
	end process;

	sync <= '1' when state = FETCH else '0'; -- the input sync signal for the both multiplier and linear approximation modules
	
	linear_aproximation_inst : linear_aproximation
	generic map (
		  N => N
		, INT => INT
		, FRC => FRC
		, family=> family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, C0 => C0
		, C1 => C1
		, H => H
		, data_i => data
		, sync_i => sync
		, data_o => result_linear_aproximation
		, sync_o => synco_linear_aproximation
	);

	weight_mul : mult35x35_pipe
	generic map (
		  family => family
	)
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => data35--to_slv(data)
		, data1_i => weight35--to_slv(weight)
		, sync_i => sync
		, data_o => p70--result_mul_slv
		, sync_o => open
	);
	data35 <= data(INT-1) & data(INT-1) & data(INT-1) & to_slv(data);
	weight35 <= weight(INT-1) & weight(INT-1) & weight(INT-1) & to_slv(weight);
	result_mul_slv <= p70(63 downto 0);
end;