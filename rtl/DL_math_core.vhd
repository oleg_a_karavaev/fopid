library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
--library ieee_proposed;
--use ieee_proposed.fixed_pkg.all;
use ieee.fixed_pkg.all;

entity DL_math_core is
generic (
	  N : integer := 64     -- A number of the weights - i.e. delay stages
	; INT : integer := 8
	; FRC : integer := 24
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic
	; valid_i : in std_logic -- data valid strobe
	; ready_o : out std_logic  -- a data ready strobe
	; data_i : in sfixed(INT-1 downto -FRC)
	; weight_i : in sfixed(INT-1 downto -FRC)
	; data_o : out sfixed(2*INT-1 downto -2*FRC)
);
end entity DL_math_core;

architecture rtl of DL_math_core is

	component linear_aproximation is
	generic (
		  N : integer := 64
		; INT : integer := 8
		; FRC : integer := 24
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; C0 : in sfixed(INT-1 downto -FRC)
		; C1 : in sfixed(INT-1 downto -FRC)
		; H : in sfixed(INT-1 downto -FRC)
		; data_i : in sfixed(INT-1 downto -FRC)
		; sync_i : in std_logic
		; data_o : in sfixed(INT-1 downto -FRC)
		; sync_o : out std_logic
	);
	end component;

	component mult32x32_pipe is -- DSP-slice based multiplier 32x32
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(31 downto 0)
		; data1_i: in std_logic_vector(31 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(63 downto 0)
		; sync_o: out std_logic
	);
	end component mult32x32_pipe;

	type fetch_state_t is (IDLE, FETCH);
	signal fetch_state : fetch_state_t := IDLE;
	signal state : state_t := IDLE;

	signal data, weight, result_mul_ff3, result_mul_ff2, result_mul_ff1, result_mul, result_linear_aproximation : sfixed(INT-1 downto -FRC);
	signal synco_linear_aproximation : std_logic;
	signal synco_linear_aproximation_ff : std_logic := '0';
	signal result_data, acc : sfixed(2*INT-1 downto -2*FRC);

begin

	data_o <= acc;

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			sync <= '0';
			synco_linear_aproximation_ff <= synco_linear_aproximation;
			ready_o <= synco_linear_aproximation_ff;
			result_mul_ff3 <= result_mul_ff2;
			result_mul_ff2 <= result_mul_ff1;
			result_mul_ff1 <= result_mul;
			-- FSM switch control
			case state is
				when IDLE =>
					if valid_i = '1' then
						state <= FETCH;
					end if;
				when FETCH =>
					if valid_i = '0'then
						state <= IDLE;
					end if;
				when others =>
					state <= IDLE;
			end case;

			-- FSM based control
			case state is
				when IDLE =>
					job <= '0';
				when FETCH =>
					job <= '1';
					data <= data_i;
					weight <= weight_i;
					sync <= '1';
				when others =>
					null;
			end case;

			result_data <= resize(result_linear_aproximation + result_mul_ff3, 2*INT-1, -2*FRC);
			if synco_linear_aproximation_ff = '1' then
				acc <= acc + result_data;
			end if;
			if clr_i = '1' then
				job <= '0';
				acc <= to_sfixed(0, 2*INT-1, -2*FRC);
				state <= IDLE;
			end if;
		end if;
	end process;
	

end;