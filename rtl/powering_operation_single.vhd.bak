-- This module implements function 1/(h^argument)
-- h - a parameter from the generic section 
-- argument - is the port argument_i
-- fixed point implementation
-- Author: Oleg Karavaev

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
--library ieee_proposed;
use ieee.fixed_pkg.all;
use std.textio.all; 

entity powering_operation_single is
generic (
	  INT :integer := 8   -- int part size of fixed point value
	; FRC :integer := 24  -- fractional part size
	; h : sfixed(7 downto -24) := to_sfixed(-4.6051701859880913680359829093687, 7, -24) -- This is a natural logarithm of what need to be powered
	; VERSION : integer := 0 -- a version of the implementation: either exponent-only module with better accuracy and bigger latency (up to 2 times), or dual (ln or exp) module with less accuracy and less latency
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic                      -- synchronous clear/reset
	; start_i : in std_logic                    -- this pulse need to start the computation
	; ready_o : out std_logic                   -- this output pulse generated when the computation is ready
	; argument_i : in sfixed(INT-1 downto -FRC) -- the power of h
	; result_o : out sfixed(INT-1 downto -FRC)  -- the result
);
end powering_operation_single;


architecture rtl of powering_operation_single is

	component taylor_exp is
	generic (
		  IN_INT :integer :=8
		; IN_FRC :integer :=24
		; OUT_INT:integer :=8
		; OUT_FRC:integer :=24
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; start_i : in std_logic
		; ready_o : out std_logic
		; argument_i : in sfixed(IN_INT-1 downto -IN_FRC)
		; result_o : out sfixed(OUT_INT-1 downto -OUT_FRC)
	);
	end component;

	component taylor_select is
	generic (
		  IN_INT :integer :=8
		; IN_FRC :integer :=24
		; OUT_INT:integer :=8
		; OUT_FRC:integer :=24
	);
	port (
		  clk_i : in std_logic
		; clr_i : in std_logic
		; start_i : in std_logic
		; ready_o : out std_logic
		; argument_i : in sfixed(IN_INT-1 downto -IN_FRC)
		; operation_i : in std_logic
		; result_o : out sfixed(OUT_INT-1 downto -OUT_FRC)
	);
	end component;

	component mult32x32 is -- DSP-slice based multiplier 32x32
	port(
		  clk_i: in std_logic
		; clr_i : in std_logic
		; data0_i: in std_logic_vector(31 downto 0)
		; data1_i: in std_logic_vector(31 downto 0)
		; sync_i: in std_logic
		; data_o: out std_logic_vector(63 downto 0)
		; sync_o: out std_logic
	);
	end component mult32x32;

	signal arg_mul_ln_sfixed : sfixed(INT-1 downto -FRC);
	signal arg_ready, arg_subtract2_ready : std_logic := '0';
	signal arg_mul_ln : std_logic_vector(63 downto 0);
	signal arg_mul_ln_subtract2 : sfixed(2*INT-1 downto -2*FRC);
	signal argument : sfixed(INT-1 downto -FRC);
	signal start_ff : std_logic_vector(3 downto 0) := "0000";
	signal pow_rdy : std_logic;
	signal job : std_logic := '0';
	signal taylor_ready : std_logic;
	signal taylor_result : sfixed(INT-1 downto -FRC);
	signal pow_result : std_logic_vector(63 downto 0);
	signal h_sized : sfixed(INT-1 downto -FRC);
begin

	h_sized <= resize(h, INT-1, -FRC);
	ready_o <= pow_rdy;

	-- A main idea: 1/(h^a) = h^(-a) = exp(-a*ln(h)), exp(x) = 1 + x + x^2/2! + x^3/3! + ...
	-- But there is problem when -a*ln(h) is bigger then 3. The Taylor series of the exponential function need a lot of elements to have even coarse accuracy
	-- To implement all these element need both more memory and more bit width (because of fixed point)
	-- However exp(a+b) = exp(a)*exp(b), so I do this: f(a) = exp(2)*exp(-a*ln(h)-2) and all becaome simpler.
	-- This is valid only in the current task. Because of the maximum argument of the exp-function is about 4.6
	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if start_i = '1' and job = '0' then
				argument <= -(resize(argument_i, INT-2, -FRC)); -- 1/h^a is equal to h^(-a)
				job <= '1';
				start_ff <= "0001";
			end if;
			if job = '1' then
				start_ff <= start_ff(2 downto 0) & '0';
				if pow_rdy = '1' then
					job <= '0';
				end if;
				if arg_ready = '1' then -- f(a) = exp(2)*exp(-a*ln(h)-2). There is -a*ln(h)-2
					arg_mul_ln_subtract2 <= to_sfixed(arg_mul_ln(62 downto 0), 2*INT-2, -2*FRC) - to_sfixed(2.0, 2*INT-2, -2*FRC);
				end if;
				arg_subtract2_ready <= arg_ready;
			end if;
			if clr_i = '1' then
				job <= '0';
				start_ff <= (others => '0');
			end if;
		end if;
	end process;

	-- 1/(h^a) = h^(-a) = exp(-a*ln(h)) so I need to multiply argument by ln(h)
	mul_to_ln : mult32x32
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => to_slv(argument)
		, data1_i => to_slv(h_sized)
		, sync_i => start_ff(3)
		, data_o => arg_mul_ln
		, sync_o => arg_ready
	);

	exp_only_version: if VERSION = 0 generate
		-- exponent-only module with better accuracy and bigger latency (up to 2 times). 28+26*N clocks, where N - is a number of elements in the Taylor series
		taylor_series_exp : taylor_exp 
		generic map (
			  IN_INT => INT
			, IN_FRC => FRC
			, OUT_INT => INT
			, OUT_FRC => FRC
		)
		port map (
			  clk_i => clk_i
			, clr_i => clr_i or start_i
			, start_i => arg_subtract2_ready
			, ready_o => taylor_ready
			, argument_i => arg_mul_ln_sfixed
			, result_o => taylor_result
		);
	end generate;

	dual_version: if VERSION = 1 generate
		-- dual (ln or exp) module with less accuracy and less latency. 15+13*N clocks, where N - is a number of elements in the Taylor series
		taylor_series_exp : taylor_select
		generic map (
			  IN_INT => INT
			, IN_FRC => FRC
			, OUT_INT => INT
			, OUT_FRC => FRC
		)
		port map (
			  clk_i => clk_i
			, clr_i => clr_i or start_i
			, start_i => arg_subtract2_ready
			, ready_o => taylor_ready
			, argument_i => arg_mul_ln_sfixed
			, operation_i => '1'
			, result_o => taylor_result
		);
	end generate;

	-- f(a) = exp(2)*exp(-a*ln(h)-2). There is a multiplication by exp(2)
	final_mul : mult32x32
	port map (
		  clk_i => clk_i
		, clr_i => clr_i
		, data0_i => to_slv(taylor_result)
		, data1_i => to_slv(to_sfixed(7.389056098930650227230427460575, INT-1, -FRC))
		, sync_i => taylor_ready
		, data_o => pow_result
		, sync_o => pow_rdy
	);
	

	arg_mul_ln_sfixed <= resize(arg_mul_ln_subtract2, INT-1, -FRC);
	result_o <= resize(to_sfixed(pow_result, 2*INT-1, -2*FRC), INT-1, -FRC);

end;