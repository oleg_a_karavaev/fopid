-- A 59x59 signed Multiplier. ug193.pdf page 71

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;


entity mult59x59 is
port(
	  clk_i: in std_logic
	; clr_i : in std_logic
	; data0_i: in std_logic_vector(58 downto 0)
	; data1_i: in std_logic_vector(58 downto 0)
	; sync_i: in std_logic
	; data_o: out std_logic_vector(117 downto 0)
	; sync_o: out std_logic
);
end entity;

architecture rtl of mult59x59 is
	
	component DSP48E is
	generic (
		  ACASCREG : integer :=  1      
		; ALUMODEREG : integer :=  1     
		; AREG : integer :=  1
		--; AUTORESET_PATTERN_DETECT : string :=  "FALSE"
		; AUTORESET_PATTERN_DETECT : boolean := FALSE
		; AUTORESET_PATTERN_DETECT_OPTINV : string :=  "MATCH"
		; A_INPUT : string :=  "DIRECT"
		; BCASCREG : integer :=  1
		; BREG : integer :=  1
		; B_INPUT : string :=  "DIRECT"
		; CARRYINREG : integer :=  1
		; CARRYINSELREG : integer := 1
		; CREG : integer := 1
		; MASK : bit_vector := X"3FFFFFFFFFFF"
		; MREG : integer := 1
		; MULTCARRYINREG : integer :=  1
		; OPMODEREG : integer := 1
		; PATTERN : bit_vector := X"000000000000"
		; PREG : integer := 1
		; SEL_MASK : string :=  "MASK"
		; SEL_PATTERN : string :=  "PATTERN"
		; SEL_ROUNDING_MASK : string :=  "SEL_MASK"
		; USE_MULT : string :=  "MULT_S"
		; USE_PATTERN_DETECT : string :=  "NO_PATDET"
		; USE_SIMD : string :=  "ONE48"
	);
	port (
		  A : in std_logic_vector(29 downto 0)
		; B : in std_logic_vector(17 downto 0)
		; C : in std_logic_vector(47 downto 0)
		; OPMODE : in std_logic_vector(6 downto 0)
		; ALUMODE : in std_logic_vector(3 downto 0)
		; CARRYIN : in std_logic
		; CARRYINSEL : in std_logic_vector(2 downto 0)
		; CEA1 : in std_logic
		; CEA2 : in std_logic
		; CEB1 : in std_logic
		; CEB2 : in std_logic
		; CEC : in std_logic
		; CEM : in std_logic
		; CEP : in std_logic
		; CEALUMODE : in std_logic
		; CECTRL : in std_logic
		; CEMULTCARRYIN : in std_logic
		; CECARRYIN : in std_logic
		; RSTA : in std_logic
		; RSTB : in std_logic
		; RSTC : in std_logic
		; RSTM : in std_logic
		; RSTP : in std_logic
		; RSTCTRL : in std_logic
		; RSTALLCARRYIN : in std_logic
		; CLK : in std_logic
		; RSTALUMODE : in std_logic
		; ACIN : in std_logic_vector(29 downto 0)
		; BCIN : in std_logic_vector(17 downto 0)
		; PCIN : in std_logic_vector(47 downto 0)
		; CARRYCASCIN : in std_logic
		; MULTSIGNIN : in std_logic
		; ACOUT : out std_logic_vector(29 downto 0)
		; BCOUT : out std_logic_vector(17 downto 0)
		; PCOUT : out std_logic_vector(47 downto 0)
		; P : out std_logic_vector(47 downto 0)
		; CARRYOUT : out std_logic_vector(3 downto 0)
		; CARRYCASCOUT : out std_logic
		; MULTSIGNOUT : out std_logic
		; PATTERNDETECT : out std_logic
		; PATTERNBDETECT : out std_logic
		; OVERFLOW : out std_logic
		; UNDERFLOW : out std_logic
	);
	end component;
	
	signal DSP1_ACOUT, DSP2_ACOUT, DSP3_ACOUT, DSP4_ACOUT : std_logic_vector(29 downto 0);
	signal DSP1_BCOUT, DSP2_BCOUT, DSP3_BCOUT, DSP4_BCOUT : std_logic_vector(17 downto 0);
	signal DSP1_PCOUT, DSP2_PCOUT, DSP3_PCOUT, DSP4_PCOUT : std_logic_vector(47 downto 0);
	signal DSP1_P, DSP2_P, DSP3_P, DSP4_P : std_logic_vector(47 downto 0);
	signal DSP5_ACOUT, DSP6_ACOUT, DSP7_ACOUT, DSP8_ACOUT : std_logic_vector(29 downto 0);
	signal DSP5_BCOUT, DSP6_BCOUT, DSP7_BCOUT, DSP8_BCOUT : std_logic_vector(17 downto 0);
	signal DSP5_PCOUT, DSP6_PCOUT, DSP7_PCOUT, DSP8_PCOUT : std_logic_vector(47 downto 0);
	signal DSP5_P, DSP6_P, DSP7_P, DSP8_P : std_logic_vector(47 downto 0);
	signal DSP9_ACOUT, DSP10_ACOUT : std_logic_vector(29 downto 0);
	signal DSP9_BCOUT, DSP10_BCOUT : std_logic_vector(17 downto 0);
	signal DSP9_PCOUT, DSP10_PCOUT : std_logic_vector(47 downto 0);
	signal DSP9_P, DSP10_P : std_logic_vector(47 downto 0);

	function replicate (input : in std_logic; size : in positive) return std_logic_vector is
		variable r : std_logic_vector(size-1 downto 0);
	begin
		for i in r'range loop
			r(i) := input;
		end loop;
		return r;
	end function;

	type reg_delay_t is array (integer range<>) of std_logic_vector(58 downto 0);

	signal data0_ff, data1_ff : reg_delay_t(0 to 8) := (others => (others => '0'));

	signal DSP1_PCOUT_ff1, DSP1_PCOUT_ff2, DSP1_PCOUT_ff3, DSP2_PCOUT_ff1, DSP2_PCOUT_ff2, DSP3_PCOUT_ff1 : std_logic_vector(47 downto 0) := (others => '0');
	signal sync_delay_line : std_logic_vector(11 downto 0) := (others => '0');
	
	signal DSP1_A, DSP2_A, DSP3_A, DSP4_A, DSP5_A, DSP6_A, DSP7_A, DSP8_A, DSP9_A, DSP10_A : std_logic_vector(29 downto 0);
	signal DSP1_B, DSP2_B, DSP3_B, DSP4_B, DSP5_B, DSP6_B, DSP7_B, DSP8_B, DSP9_B, DSP10_B : std_logic_vector(17 downto 0);
	constant ZERO30 : std_logic_vector(29 downto 0) := (others=> '0');
	constant ZERO48 : std_logic_vector(47 downto 0) := (others=> '0');
	constant ZERO18 : std_logic_vector(17 downto 0) := (others=> '0');
	
begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			data0_ff(0) <= data0_i;
			data1_ff(0) <= data1_i;
			for i in 1 to 8 loop
				data0_ff(i) <= data0_ff(i-1);
				data1_ff(i) <= data1_ff(i-1);
			end loop;
			DSP1_PCOUT_ff1 <= DSP1_P;
			DSP1_PCOUT_ff2 <= DSP1_PCOUT_ff1;
			DSP1_PCOUT_ff3 <= DSP1_PCOUT_ff2;
			DSP2_PCOUT_ff1 <= DSP2_P;
			DSP2_PCOUT_ff2 <= DSP2_PCOUT_ff1;
			DSP3_PCOUT_ff1 <= DSP3_P;
			sync_delay_line <= sync_delay_line(sync_delay_line'high-1 downto 0) & sync_i;
			if clr_i = '1' then
				sync_delay_line <= (others => '0');
			end if;
		end if;
	end process;

	sync_o <= sync_delay_line(sync_delay_line'high);

	data_o <= DSP10_P(32 downto 0) & DSP9_P(16 downto 0) & DSP8_P(16 downto 0) & DSP6_P(16 downto 0) & DSP3_P(16 downto 0) & DSP1_P(16 downto 0);

	DSP1_A <= "00000" & x"00" & data0_i(16 downto 0);
	DSP1_B <= '0' & data1_i(16 downto 0);
	
	DSP48E_1 : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 1
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 1       
		, BREG => 1
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP1_A
		, B => DSP1_B
		, C => ZERO48
		, OPMODE => "0000101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => ZERO48
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP1_ACOUT
		, BCOUT => DSP1_BCOUT
		, PCOUT => DSP1_PCOUT
		, P => DSP1_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP2_A <= "00000" & x"00" & data0_i(33 downto 17);
	
	DSP48E_2 : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 1       
		, BREG => 1
		, B_INPUT => "CASCADE"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP2_A
		  --A => replicate('0', 10+5) & data0_i(31 downto 17)
		, B => ZERO18
		, C => ZERO48
		, OPMODE => "1010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => DSP1_BCOUT
		, PCIN => DSP1_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP2_ACOUT
		, BCOUT => DSP2_BCOUT
		, PCOUT => DSP2_PCOUT
		, P => DSP2_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP3_A <= "00000" & x"00" & data0_ff(0)(16 downto 0);
	DSP3_B <= '0' & data1_ff(0)(33 downto 17);
	
	DSP48E_3 : DSP48E
	generic map (
		  ACASCREG => 2       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 2       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP3_A
		, B => DSP3_B
		--, B => replicate('0', 3) & data1_ff1(31 downto 17)
		, C => ZERO48
		, OPMODE => "0010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => DSP2_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP3_ACOUT
		, BCOUT => DSP3_BCOUT
		, PCOUT => DSP3_PCOUT
		, P => DSP3_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP4_A <= "00000" & x"00" & data0_ff(1)(33 downto 17);
	 
	DSP48E_4 : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 1       
		, BREG => 1
		, B_INPUT => "CASCADE"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP4_A
		  --A => replicate('0', 10+5) & data0_ff2(31 downto 17)
		, B => ZERO18
		, C => ZERO48
		, OPMODE => "1010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => DSP3_BCOUT
		, PCIN => DSP3_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP4_ACOUT
		, BCOUT => DSP4_BCOUT
		, PCOUT => DSP4_PCOUT
		, P => DSP4_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP5_A <= replicate(data0_ff(2)(58), 5) & data0_ff(2)(58 downto 34);
	DSP5_B <= '0' & data1_ff(2)(16 downto 0);

	DSP48E_5 : DSP48E
	generic map (
		  ACASCREG => 2       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 2       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP5_A
		, B => DSP5_B
		--, B => replicate('0', 3) & data1_ff1(31 downto 17)
		, C => ZERO48
		, OPMODE => "0010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => DSP4_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP5_ACOUT
		, BCOUT => DSP5_BCOUT
		, PCOUT => DSP5_PCOUT
		, P => DSP5_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP6_A <= replicate(data1_ff(3)(58), 5) & data1_ff(3)(58 downto 34);
	DSP6_B <= '0' & data0_ff(3)(16 downto 0);
	
	DSP48E_6 : DSP48E
	generic map (
		  ACASCREG => 2       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 2       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP6_A
		, B => DSP6_B
		--, B => replicate('0', 3) & data1_ff1(31 downto 17)
		, C => ZERO48
		, OPMODE => "0010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => DSP5_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP6_ACOUT
		, BCOUT => DSP6_BCOUT
		, PCOUT => DSP6_PCOUT
		, P => DSP6_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);
	
	DSP7_B <= '0' & data0_ff(4)(33 downto 17);

	DSP48E_7 : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 1
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "CASCADE"
		, BCASCREG => 1       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => ZERO30
		  --A => replicate('0', 10+5) & data0_ff2(31 downto 17)
		, B => DSP7_B
		, C => ZERO48
		, OPMODE => "1010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => DSP6_ACOUT
		, BCIN => ZERO18
		, PCIN => DSP6_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP7_ACOUT
		, BCOUT => DSP7_BCOUT
		, PCOUT => DSP7_PCOUT
		, P => DSP7_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP8_A <= replicate(data0_ff(5)(58), 5) & data0_ff(5)(58 downto 34);
	DSP8_B <= '0' & data1_ff(5)(33 downto 17);

	DSP48E_8 : DSP48E
	generic map (
		  ACASCREG => 2       
		, ALUMODEREG => 0
		, AREG => 2
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "DIRECT"
		, BCASCREG => 2       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => DSP8_A
		, B => DSP8_B
		--, B => replicate('0', 3) & data1_ff1(31 downto 17)
		, C => ZERO48
		, OPMODE => "0010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => ZERO30
		, BCIN => ZERO18
		, PCIN => DSP7_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP8_ACOUT
		, BCOUT => DSP8_BCOUT
		, PCOUT => DSP8_PCOUT
		, P => DSP8_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP9_B <= '0' & data1_ff(6)(50 downto 34);

	DSP48E_9 : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 1
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "CASCADE"
		, BCASCREG => 1       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => ZERO30
		  --A => replicate('0', 10+5) & data0_ff2(31 downto 17)
		, B => DSP9_B
		, C => ZERO48
		, OPMODE => "1010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => DSP8_ACOUT
		, BCIN => ZERO18
		, PCIN => DSP8_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP9_ACOUT
		, BCOUT => DSP9_BCOUT
		, PCOUT => DSP9_PCOUT
		, P => DSP9_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);

	DSP10_B <= replicate(data1_ff(7)(58), 10) & data1_ff(7)(58 downto 51);

	DSP48E_10 : DSP48E
	generic map (
		  ACASCREG => 1       
		, ALUMODEREG => 0
		, AREG => 1
		, AUTORESET_PATTERN_DETECT => FALSE--"FALSE"
		, AUTORESET_PATTERN_DETECT_OPTINV => "MATCH"
		, A_INPUT => "CASCADE"
		, BCASCREG => 1       
		, BREG => 2
		, B_INPUT => "DIRECT"
		, CARRYINREG => 0
		, CARRYINSELREG => 0
		, CREG => 0
		, MASK =>  X"3FFFFFFFFFFF"
		, MREG => 1
		, MULTCARRYINREG => 0
		, OPMODEREG => 0
		, PATTERN => X"000000000000"
		, PREG =>  1
		, SEL_MASK => "MASK"
		, SEL_PATTERN => "PATTERN"
		, SEL_ROUNDING_MASK => "SEL_MASK"
		, USE_MULT => "MULT_S"
		, USE_PATTERN_DETECT => "NO_PATDET"
		, USE_SIMD => "ONE48"
	)
	port map (
		  A => ZERO30
		  --A => replicate('0', 10+5) & data0_ff2(31 downto 17)
		, B => DSP10_B
		, C => ZERO48
		, OPMODE => "1010101"
		, ALUMODE => "0000"
		, CARRYIN => '0'
		, CARRYINSEL => "000"
		, CEA1 => '1'
		, CEA2 => '1'
		, CEB1 => '1'
		, CEB2 => '1'
		, CEC => '1'
		, CEM => '1'
		, CEP => '1'
		, CEALUMODE => '1'
		, CECTRL => '1'
		, CEMULTCARRYIN => '1'
		, CECARRYIN => '1'
		, RSTA => '0'
		, RSTB => '0'
		, RSTC => '0'
		, RSTM => '0'
		, RSTP => '0'
		, RSTCTRL => '0'
		, RSTALLCARRYIN => '0'
		, CLK => clk_i
		, RSTALUMODE => '0'
		, ACIN => DSP9_ACOUT
		, BCIN => ZERO18
		, PCIN => DSP9_PCOUT
		, CARRYCASCIN => '0'
		, MULTSIGNIN => '0'
		, ACOUT => DSP10_ACOUT
		, BCOUT => DSP10_BCOUT
		, PCOUT => DSP10_PCOUT
		, P => DSP10_P
		, CARRYOUT => open
		, CARRYCASCOUT => open
		, MULTSIGNOUT => open
		, PATTERNDETECT => open
		, PATTERNBDETECT => open
		, OVERFLOW => open
		, UNDERFLOW => open
	);	

end architecture;

