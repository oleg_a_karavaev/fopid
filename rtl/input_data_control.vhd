library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ieee_proposed;
use ieee_proposed.fixed_pkg.all;
--use ieee.fixed_pkg.all;

entity data_feeding_control is
generic (
	  N : integer := 64     -- A number of the weights - i.e. delay stages
	; INT : integer := 8
	; FRC : integer := 24
);
port (
	  clk_i : in std_logic
	; clr_i : in std_logic
	; data_pscl_i : in std_logic
	; ready_o : out std_logic -- is available to receive a next data
	; start_i : in std_logic  -- start to work
	; data_i : in sfixed(INT-1 downto -FRC)
	; complete_o : out std_logic -- data feeding to an extarnal module is complete
	; ready_i : in std_logic  -- an external module ready to get a new data
	; valid_o : out std_logic -- data valid strobe
	; data_o : out sfixed(INT-1 downto -FRC)
);
end entity;

architecture rtl of data_feeding_control is

	type data_container_t is array (integer range <>) of sfixed(INT-1 downto -FRC);
	signal data_container : data_container_t(0 to N-1) := (others => to_sfixed(0.0, INT-1, -FRC));
	constant SIZE : integer := integer(log2(real(N)));
	signal data_input_cnt : unsigned(SIZE-1 downto 0) := (others => '0');
	signal data_output_cnt : unsigned(SIZE-1 downto 0) := (others => '0');
	signal empty : std_logic;
	signal job : std_logic := '0';
	type state_t is (IDLE, SEND_DATA, WAIT_READY);
	signal state : state_t := IDLE;
begin

	empty <= '1' when to_integer(data_input_cnt) = to_unsigned(N-1, SIZE) else '0';

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			ready_o <= not empty;
			valid_o <= '0';
			complete_o <= '0';
			data_o <= to_sfixed(data_container(to_integer(data_output_cnt)), INT-1, -FRC);
			if start_i = '1' then
				data_output_cnt <= (others => '0');
				job <= '1';
				state <= WAIT_READY;
			end if;
			if job = '1' then
				case state is
					when WAIT_READY =>
						if ready_i = '1'  then
							state <= SEND_DATA;
						end if;
					when SEND_DATA =>
						valid_o <= '1';
						if data_output_cnt = to_unsigned(N-1, SIZE) then
							state <= IDLE;
						else
							state <= WAIT_READY;
							data_output_cnt <= data_output_cnt + 1;
						end if;
					when IDLE =>
						job <= '0';
						complete_o <= '1';
						data_input_cnt <= (others=> '0');
					when others =>
						state <= IDLE;
				end case;
			end if;
			if empty = '0' then
				if data_pscl_i = '1' then
					data_container(to_integer(data_input_cnt)) <= to_slv(data_i);
					data_input_cnt <= data_input_cnt + 1;
				end if;
			end if;
		end if;
	end process;

end;